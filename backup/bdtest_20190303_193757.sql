-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- DROP DATABASE "bdtest" ----------------------------------
DROP DATABASE IF EXISTS `bdtest`;
-- ---------------------------------------------------------


-- CREATE DATABASE "bdtest" --------------------------------
CREATE DATABASE `bdtest` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bdtest`;
-- ---------------------------------------------------------


-- CREATE TABLE "tbcargo" --------------------------------------
CREATE TABLE `tbcargo` ( 
	`id` Int( 3 ) AUTO_INCREMENT NOT NULL,
	`cargo` VarChar( 60 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_spanish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 40;
-- -------------------------------------------------------------


-- CREATE TABLE "tbpersonas" -----------------------------------
CREATE TABLE `tbpersonas` ( 
	`idper` Int( 10 ) AUTO_INCREMENT NOT NULL,
	`cedula` Float( 12, 0 ) NOT NULL,
	`nombre` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
	`sexo` VarChar( 10 ) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
	`cargo_id` Int( 3 ) NULL,
	`direccion` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
	`correo` VarChar( 60 ) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
	`tlfnocel` VarChar( 14 ) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
	`tlfnocas` VarChar( 14 ) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
	`statusper` VarChar( 14 ) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
	`fechain` Date NOT NULL,
	`operador` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
	PRIMARY KEY ( `idper` ),
	CONSTRAINT `cedindex` UNIQUE( `cedula` ),
	CONSTRAINT `correo` UNIQUE( `correo` ),
	CONSTRAINT `idper` UNIQUE( `idper` ) )
CHARACTER SET = utf8
COLLATE = utf8_spanish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 202;
-- -------------------------------------------------------------


-- Dump data of "tbcargo" ----------------------------------
INSERT INTO `tbcargo`(`id`,`cargo`) VALUES 
( '1', 'Profesor' ),
( '2', 'Desarrollador' ),
( '3', 'Diseñador' ),
( '4', 'Analista' ),
( '5', 'Encargado de Local Comercial' ),
( '6', 'Encargado de Stock Comercio Minorista' ),
( '7', 'Asistente de Escrituraciones' ),
( '8', 'Encuestador Área Marketing' ),
( '9', 'Merchandiser' ),
( '10', 'Ejecutivo de Ventas' ),
( '11', 'Asistente de Marketing' ),
( '12', 'Gerente de Ventas' ),
( '13', 'Ejecutivo de Ventas' ),
( '14', 'Gerente de Canal' ),
( '15', 'Analista de Marketing' ),
( '16', 'Promotor' ),
( '17', 'Encuestador Área Marketing' ),
( '18', 'Gerente de Ventas' ),
( '19', 'Gerente de Sucursal' ),
( '20', 'Supervisor de Ventas' ),
( '21', 'Analista de Ventas' ),
( '22', 'Analista de Marketing Junior' ),
( '23', 'Supervisor de Marketing' ),
( '24', 'Comercial Unidad de Negocios' ),
( '25', 'Promotor' ),
( '26', 'Telemarketero' ),
( '27', 'Administrativo Área Ventas' ),
( '28', 'Supervisor de Telemarketing' ),
( '29', 'Promotor' ),
( '30', 'Analista de Marketing' ),
( '31', 'Ejecutivo de Marketing' ),
( '32', 'Desarrollador de Negocios' ),
( '33', 'Investigador de Mercado' ),
( '34', 'Encargado de Local Comercial' ),
( '35', 'Encargado de Local Comercial' ),
( '36', 'Investigador de Mercado' ),
( '37', 'Ejecutivo de Ventas' ),
( '38', 'Promotor' ),
( '39', 'Vendedor de Salón' );
-- ---------------------------------------------------------


-- Dump data of "tbpersonas" -------------------------------
INSERT INTO `tbpersonas`(`idper`,`cedula`,`nombre`,`sexo`,`cargo_id`,`direccion`,`correo`,`tlfnocel`,`tlfnocas`,`statusper`,`fechain`,`operador`) VALUES 
( '1', '7022', 'Lucas Pol Véliz Flores', 'MASCULINO', '32', 'Calle Jan Oliver, 29, Apto 46, Muñiz de Mara Edo. Amazonas, 1624', 'lucia.sedillo@figueroa.net.ve', '+58 262-474-94', '4023998978', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '2', '64266', 'Dr. Ignacio Deleón Hijo', 'MASCULINO', '8', 'Av. Valencia, 8, Hab. 78, Villa Mateode Altagracia Edo. Carabobo, 5978', 'serrano.abril@yahoo.es', '+58 286-154939', '+58 296 842778', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '3', '953740', 'Ing. Daniel Quintana', 'MASCULINO', '24', 'Carretera Cesar, 0, Apto 0, Valle Lunade Mata Edo. Trujillo, 3114', 'dario.mas@duran.com.ve', '481-3577581', '+58 469-136006', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '4', '271339', 'Antonia Narváez Hijo', 'MASCULINO', '6', 'Carretera Adriana Vicente, 35, Nro 05, Villa Cristian Edo. Delta Amacuro', 'ccuriel@terra.com', '4211991879', '225 214 9275', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '5', '587199', 'Gonzalo Jaime Cardenas', 'MASCULINO', '3', 'Vereda Aguirre, Apto 2, San Pedrode Altagracia Edo. Lara, 6960', 'alma71@flores.info.ve', '+58 461 930 22', '484 940 1401', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '6', '982144', 'Raquel Africa Gálvez', 'MASCULINO', '4', 'Cl. Omar, 184, Casa 23, Los Francisco Edo. Barinas, 3180', 'marc.guerra@garica.net', '+58 2138828335', '+58 404 275113', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '7', '263080', 'Rodrigo Ignacio Font', 'MASCULINO', '10', 'Av. Sergio, 591, Piso 61, Roberto del Tuy Edo. Anzoátegui, 5415', 'naiara33@munoz.org', '+58 269 422938', '484-077-5624', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '8', '926943', 'Omar Fernando Godínez Simón', 'FEMENINO', '22', 'Callejón Antonia, Casa 43, Soria de Mata Edo. Miranda, 4057', 'ane39@live.com', '428-5340859', '407-959-1047', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '9', '996082', 'Iria Villagómez', 'FEMENINO', '23', 'Av. Carlota Rivero, 11, Hab. 9, Los Santiago Edo. Trujillo, 6978', 'alejandra39@hotmail.com', '203 6910177', '+58 271-083-95', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '10', '764336', 'Domingo Cabán', 'FEMENINO', '6', 'Vereda Alma Véliz, 354, Nro 5, Aranda de Altagracia Edo. Guárico, 6085', 'mejia.izan@brito.net', '+58 4987144312', '+58 430-494-78', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '11', '914248', 'Ing. Lucia Alejandro Hijo', 'MASCULINO', '32', 'Vereda Sergio, Nro 6, La Jimena Edo. Falcón, 4718', 'tmeza@gallegos.org.ve', '408 8851800', '290 686 5569', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '12', '133623', 'Lcdo. Ignacio Paz', 'MASCULINO', '24', 'Carretera Abrego, 7, Hab. 74, Los Silvia Edo. Trujillo, 2462', 'alicia.ocasio@yahoo.es', '218-317-3282', '2561679774', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '13', '86043', 'Ian Salvador', 'MASCULINO', '24', 'Av. Serrano, Apto 3, Delao del Valle Edo. Carabobo, 9042', 'guzman.ana@terra.com', '453 5934383', '2555784812', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '14', '698429', 'Isaac Bonilla Hijo', 'MASCULINO', '11', 'Av. Delacrúz, Piso 08, Alicia de Asis Edo. Táchira', 'irene49@gmail.com', '+58 2901662692', '408 034 4025', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '15', '471811', 'Sra. Patricia Palomino', 'FEMENINO', '23', 'Vereda Diana Vélez, 02, Apto 71, Parroquia Oliviade Mara Edo. Anzoátegui, 3626', 'bloera@hotmail.com', '+58 255 384988', '405 5013510', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '16', '866406', 'Sr. Victor Botello', 'MASCULINO', '4', 'Carretera Olivo, 14, Casa 7, Los Gonzalo Edo. Yaracuy, 1454', 'leonardo.arredondo@yahoo.com', '4165894868', '+58 276 709 70', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '17', '349858', 'Yorman Otero', 'FEMENINO', '26', 'Calle Sosa, Casa 1, Alex de Mata Edo. Cojedes, 5920', 'jcanales@linares.info.ve', '463-409-9726', '405-838-7986', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '18', '807218', 'Paola Del río', 'MASCULINO', '21', 'Vereda Navarro, Apto 5, Puerto Vega Edo. Miranda, 3718', 'segovia.nerea@aguilar.com', '+58 2832473201', '405-794-9924', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '19', '359569', 'Daniela Hernandes Hijo', 'MASCULINO', '34', 'Avenida Marta Piñeiro, 101, Casa 04, Parroquia Zulayde Asis Edo. Sucre, 9660', 'pablo.roca@gurule.org.ve', '+58 441-972121', '409 1471090', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '20', '914835', 'Adam Omar Ibarra', 'FEMENINO', '25', 'Calle Elena, Hab. 66, Villa Julia Edo. Delta Amacuro, 3895', 'kfranco@cardenas.org', '+58 219-921965', '454 8442876', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '21', '617806', 'Ona Escribano Hijo', 'FEMENINO', '8', 'Callejón Villar, 48, Nro 4, San Andresde Mata Edo. Distrito Capital', 'vverdugo@irizarry.info.ve', '277 4275683', '481-2586944', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '22', '30973', 'Cristian Erik Armendáriz Caldera', 'FEMENINO', '29', 'Av. Ruelas, Nro 4, Puerto Robertodel Tuy Edo. Falcón', 'rgarrido@hotmail.es', '+58 248 534038', '493 747 4070', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '23', '827657', 'Silvia Flórez', 'MASCULINO', '4', 'Av. Díaz, Piso 4, Puerto Domingode Altagracia Edo. Delta Amacuro, 5744', 'vera35@castellanos.com', '2584071874', '+58 245 946 16', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '24', '732386', 'Mateo Marco Garay Lugo', 'MASCULINO', '18', 'Vereda Adrian, Hab. 30, Godoy de Mata Edo. Miranda', 'paola58@live.com', '+58 467 119 62', '+58 4935310234', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '25', '738936', 'Miriam Ponce', 'FEMENINO', '3', 'Vereda Miriam Meza, Apto 87, El Carlos Edo. Yaracuy, 0597', 'gpolo@colon.org.ve', '259-890-0262', '437 089 3403', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '26', '876542', 'Paola Izquierdo', 'MASCULINO', '12', 'Callejón Patricia, 847, Hab. 5, Andres del Valle Edo. Zulia', 'ncovarrubias@sauceda.com.ve', '+58 427-188-84', '273 9687754', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '27', '923859', 'Dn. Miguel Marín Hijo', 'MASCULINO', '4', 'Vereda Adrian Carrillo, Piso 21, El Carlosde Asis Edo. Cojedes', 'drodriguez@ledesma.com', '+58 298-529044', '+58 444-938-05', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '28', '367570', 'Leo Miguel Barrientos', 'FEMENINO', '5', 'Avenida Blanca, 69, Casa 53, Adrian del Valle Edo. Barinas, 9990', 'jon.menendez@moya.com.ve', '433-064-6558', '4714566855', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '29', '884332', 'Andres Andres Almanza', 'FEMENINO', '18', 'Av. Blanca, 231, Nro 8, Antonio del Valle Edo. Bolívar, 2899', 'marcos.giron@duarte.net', '+58 2897938499', '232-527-8405', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '30', '840319', 'Candela Marta Rocha', 'FEMENINO', '35', 'Av. Patricia, 42, Piso 0, La Yorman Edo. Apure', 'esauceda@gmail.com', '453-6676540', '2337093748', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '31', '781248', 'Luis Aguilera Hijo', 'MASCULINO', '32', 'Av. Pons, Nro 6, Lola del Tuy Edo. Anzoátegui', 'franciscojavier.mesa@latinmail.com', '492-649-4901', '462 5385934', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '32', '785393', 'Elsa Reynoso', 'FEMENINO', '6', 'Carretera Aroa Rangel, 71, Nro 87, Pablo de Asis Edo. Apure, 2682', 'jose.medina@zamudio.org.ve', '220-9391273', '+58 413-001860', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '33', '426149', 'Daniel Del río Hijo', 'MASCULINO', '25', 'Calle Marco Baca, 001, Apto 2, Celia del Tuy Edo. Monagas, 4995', 'psoto@hispavista.com', '+58 258-158217', '+58 430 022 81', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '34', '732461', 'Lcdo. Omar Madrid', 'MASCULINO', '5', 'Avenida Antonia Caraballo, Casa 21, Olivas del Valle Edo. Cojedes, 3680', 'marc70@almaraz.com.ve', '+58 404-275-18', '+58 438 357 94', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '35', '917051', 'Nahia Luna Carbonell', 'FEMENINO', '7', 'Callejón Roberto Cavazos, Piso 2, Ander de Mara Edo. Monagas, 8834', 'fmiramontes@latinmail.com', '256-4229928', '407 556 3503', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '36', '182172', 'Srita. Luna Mercado', 'MASCULINO', '34', 'Vereda Angel Álvarez, 73, Nro 1, Villa Francisco Edo. Trujillo, 9240', 'alaniz.jon@yahoo.com', '+58 437 667 64', '+58 475 481 13', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '37', '345064', 'Eric Andres Haro Arredondo', 'FEMENINO', '7', 'Callejón Jesus Blasco, 20, Piso 5, Villa Daniela Edo. Mérida', 'ines.quiroz@terra.com', '+58 245-851983', '+58 447-859-61', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '38', '955423', 'Mario Pablo Becerra', 'FEMENINO', '9', 'Vereda Claudia, Hab. 9, Los Nicolas Edo. Guárico, 7471', 'marc44@cordero.info.ve', '488 627 0834', '+58 222-495891', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '39', '335381', 'Leonardo Ruben Candelaria', 'FEMENINO', '18', 'Callejón Manuela, Piso 63, Jana de Mata Edo. Apure, 0323', 'julia30@reyna.info.ve', '+58 443 649182', '+58 2746809050', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '40', '345554', 'Aleix Ismael Sanches Botello', 'FEMENINO', '13', 'Cl. Fatima, Piso 4, Villa Noeliade Altagracia Edo. Distrito Capital', 'ygalindo@fonseca.web.ve', '409-285-4440', '+58 2725938605', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '41', '719357', 'Alvaro Curiel', 'MASCULINO', '34', 'Vereda Joel, Piso 04, Villa Aroade Asis Edo. Delta Amacuro', 'chacon.ane@balderas.net.ve', '4102901588', '4178095641', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '42', '28926', 'Luis Alex Casado Mireles', 'MASCULINO', '21', 'Avenida Ona, 6, Hab. 66, Ander del Tuy Edo. Falcón, 2364', 'carlos68@caldera.net', '+58 4600866658', '255-7124942', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '43', '653860', 'Lcdo. Eric Medina Hijo', 'FEMENINO', '34', 'Callejón Juan, 5, Piso 5, Parroquia Oscarde Altagracia Edo. Falcón, 1300', 'maestas.jon@hotmail.es', '+58 4083349344', '237 5600071', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '44', '139285', 'Carlota Vélez', 'MASCULINO', '13', 'Av. Vega Piña, Apto 9, Barrientos de Mara Edo. Guárico, 8508', 'jan.exposito@hotmail.com', '+58 418 436 20', '+58 448-365535', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '45', '848164', 'Pablo Leandro Briseño', 'FEMENINO', '19', 'Vereda Oliver Sisneros, Hab. 15, Miguel de Mara Edo. Distrito Capital, 1827', 'romo.ian@moreno.org', '2781173897', '+58 2297718946', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '46', '888174', 'Srita. Raquel Villalba Hijo', 'FEMENINO', '32', 'Vereda Diego, 85, Nro 12, Sara de Mata Edo. Miranda', 'mateo.barrientos@live.com', '+58 268 322933', '271 5150404', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '47', '605192', 'Bruno Dario Puig', 'FEMENINO', '12', 'Avenida Elsa, 659, Hab. 5, Enrique de Mata Edo. Carabobo, 5643', 'ruben.godoy@cuellar.net.ve', '495-066-8068', '229-4923217', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '48', '960005', 'Marcos Barajas', 'FEMENINO', '13', 'Vereda Isaac Ojeda, 5, Piso 0, Erik del Tuy Edo. Distrito Capital, 7385', 'fernandez.victoria@ceballos.net.ve', '+58 279-646-46', '+58 265 567049', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '49', '397157', 'Sra. Claudia Mojica Hijo', 'FEMENINO', '13', 'Av. Elsa, Casa 01, Santa Dianadel Tuy Edo. Delta Amacuro', 'berta08@arredondo.com.ve', '+58 4471672689', '+58 407 843669', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '50', '737584', 'Alma Sedillo Hijo', 'FEMENINO', '3', 'Av. Morales, Casa 5, Villalba del Tuy Edo. Guárico', 'nerea57@terra.com', '253-1161293', '+58 239 451480', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '51', '123052', 'Erika Esquibel', 'FEMENINO', '2', 'Calle Miguel, Hab. 3, Santa Samuel Edo. Sucre', 'atencio.alexandra@bonilla.net', '+58 472-668-16', '255 733 1002', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '52', '733624', 'Anna Escalante Contreras', 'FEMENINO', '19', 'Vereda Urbina, 520, Piso 5, El Candela Edo. Monagas', 'valadez.aroa@live.com', '+58 259-619-87', '+58 268 182 37', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '53', '41393', 'Srita. Ines Zayas', 'FEMENINO', '18', 'Vereda Palomino, Hab. 96, Marti de Mata Edo. Aragua, 3233', 'lrangel@munoz.co.ve', '400-733-6583', '+58 4538317085', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '54', '917975', 'Juan Lomeli Hijo', 'MASCULINO', '15', 'Carretera Araña, Apto 71, Ortega de Altagracia Edo. Nueva Esparta, 3656', 'gabriela.esparza@hispavista.com', '2455245419', '+58 2297169929', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '55', '466296', 'Srita. Celia Soliz', 'MASCULINO', '23', 'Cl. Alejandra Galindo, Apto 7, Villa Nahia Edo. Táchira, 4594', 'navarrete.leonardo@ocampo.org', '405 7841034', '+58 293 062764', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '56', '126657', 'Srita. Nil Velázquez Hijo', 'FEMENINO', '30', 'Carretera Domingo, 0, Casa 09, Ariadna del Tuy Edo. Carabobo, 5404', 'diaz.ismael@tamez.co.ve', '435-210-3058', '217 718 9232', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '57', '73920', 'Nadia Puig', 'MASCULINO', '16', 'Cl. Mario Ibáñez, 5, Hab. 63, Sandra del Valle Edo. Trujillo, 2774', 'luis.valdez@vallejo.net', '265-842-1947', '+58 448 451 85', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '58', '438470', 'Laura Frías Franco', 'MASCULINO', '18', 'Av. Adam, Piso 0, Martin de Altagracia Edo. Carabobo', 'navarro.gabriela@live.com', '292 8855524', '+58 2546459331', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '59', '426943', 'Lucas Moral Hijo', 'MASCULINO', '6', 'Cl. Quiñónez, 1, Piso 46, Sáenz de Mata Edo. Guárico, 8646', 'griego.leonardo@munguia.org', '+58 453 088693', '298 9456033', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '60', '109813', 'Claudia Mireia Posada', 'MASCULINO', '10', 'Calle Acevedo, Apto 1, El Oscarde Asis Edo. Falcón, 1294', 'clara.badillo@gurule.web.ve', '+58 464-542-21', '+58 2928003887', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '61', '22699', 'Elena Arias Hijo', 'MASCULINO', '7', 'Cl. Sebastian, 964, Casa 91, Paula de Altagracia Edo. Yaracuy', 'javier.ulloa@veliz.net', '400 1457248', '+58 212 914444', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '62', '547043', 'Javier Coronado Hijo', 'FEMENINO', '3', 'Cl. Quezada, 011, Hab. 02, Valle Iriade Mata Edo. Barinas, 1297', 'pablo.meza@hispavista.com', '4056309398', '+58 219-701-14', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '63', '745137', 'Lara Carmona', 'FEMENINO', '27', 'Av. Helena, Apto 80, Vela de Altagracia Edo. Barinas', 'lucas.serrano@latinmail.com', '+58 467 776855', '+58 435 283 49', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '64', '577297', 'Teresa Santiago', 'MASCULINO', '18', 'Av. Sandra Alva, Nro 69, Carolina de Asis Edo. Mérida', 'uperez@yahoo.es', '+58 414-544-91', '+58 428 550569', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '65', '710341', 'Leire Sara Corral', 'FEMENINO', '12', 'Avenida Silvia Villar, 8, Casa 64, Olivia del Valle Edo. Bolívar, 8898', 'cmendez@yahoo.es', '+58 213 160 77', '426 994 2397', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '66', '25782', 'Yorman Soliz', 'FEMENINO', '6', 'Callejón Francisco Javier, 363, Casa 7, Santa Ruben Edo. Barinas', 'rocio.santana@gmail.com', '+58 439 809028', '+58 264-824-43', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '67', '342543', 'Lcda. Fatima Calderón Hijo', 'FEMENINO', '27', 'Calle Font, Apto 7, Las Cesar Edo. Sucre, 4420', 'nayara.lorenzo@yahoo.com', '294-079-6468', '244 8916187', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '68', '538548', 'Nora Raquel Arribas', 'FEMENINO', '6', 'Calle Elena, Nro 23, Izan de Asis Edo. Lara, 9588', 'uaguirre@ceballos.net.ve', '+58 435-917-47', '+58 4145012501', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '69', '94694', 'Lcda. Natalia Delapaz Hijo', 'FEMENINO', '9', 'Carretera Ocampo, Apto 17, Puerto Pol Edo. Guárico', 'jose65@hotmail.es', '+58 4865577523', '+58 427 488 82', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '70', '595100', 'Javier Collado Hijo', 'FEMENINO', '8', 'Avenida Silvia Aparicio, 435, Apto 5, Orta del Tuy Edo. Miranda, 8324', 'altamirano.julia@terra.com', '2674891847', '471-179-8863', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '71', '454959', 'Leyre Aguilar Hijo', 'FEMENINO', '24', 'Callejón Amador, 6, Piso 11, Naia de Altagracia Edo. Trujillo, 8147', 'marc79@florez.com', '222 567 4134', '200 2526405', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '72', '13079', 'Sr. Joel Beltrán', 'MASCULINO', '1', 'Avenida Paola, Piso 1, Valle Alba Edo. Zulia', 'ivan77@latinmail.com', '+58 225-610-85', '+58 4094267531', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '73', '711469', 'Victoria Leyre Quiñónez', 'FEMENINO', '1', 'Calle Anaya, 18, Nro 89, Parroquia Carmen Edo. Zulia', 'laura02@hotmail.com', '+58 277 464 18', '+58 247-685391', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '74', '402084', 'Srita. Lidia Roldan Hijo', 'MASCULINO', '29', 'Av. Jaime Muro, 0, Casa 85, Estrada de Mata Edo. Delta Amacuro, 7279', 'jesparza@cabrera.com.ve', '458 516 1789', '+58 435 911 50', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '75', '598313', 'Lola Samaniego Hijo', 'MASCULINO', '16', 'Avenida Porras, Hab. 3, Olivárez de Altagracia Edo. Portuguesa, 5710', 'imoral@mena.info.ve', '245 8534745', '+58 490 007685', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '76', '321943', 'Fernando Arce Hijo', 'FEMENINO', '13', 'Callejón Mateo Solorzano, 3, Piso 9, Santiago del Valle Edo. Distrito Capital, 9888', 'santiago.oswaldo@hotmail.es', '+58 271-996442', '+58 4971011928', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '77', '631891', 'Ona Valero', 'MASCULINO', '29', 'Callejón Nora Palacios, 30, Apto 93, Laia de Altagracia Edo. Yaracuy', 'concepcion.eva@gmail.com', '+58 4983194288', '+58 253 358 57', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '79', '161288', 'Teresa Carmona', 'FEMENINO', '30', 'Calle Erika, 53, Nro 4, Pol de Mata Edo. Trujillo', 'montero.alex@yahoo.com', '+58 472 933 58', '219-9785609', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '80', '236205', 'Rayan Longoria Hijo', 'MASCULINO', '16', 'Vereda Fatima Carvajal, Piso 90, Botello de Mata Edo. Guárico', 'viera.carlos@gmail.com', '205-0323687', '4969606208', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '81', '594770', 'Dra. Zulay Lucio', 'MASCULINO', '5', 'Avenida Rocio, 4, Apto 13, Pol de Asis Edo. Trujillo', 'carla75@godinez.org', '437-932-7441', '+58 2924998406', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '82', '843438', 'Sra. Candela Cruz', 'FEMENINO', '22', 'Cl. Beatriz Mata, Hab. 4, Samuel de Altagracia Edo. Apure, 4122', 'castillo.nuria@valentin.net', '290-251-2577', '+58 243-739599', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '83', '823112', 'Sr. Pedro Urbina', 'FEMENINO', '18', 'Cl. Izan, 1, Hab. 06, Los Ivan Edo. Táchira, 4061', 'alejandro24@gmail.com', '4106046107', '+58 247 434533', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '84', '546650', 'Alvaro Marcos Trujillo', 'FEMENINO', '7', 'Avenida Rael, 2, Nro 85, Gallardo de Mata Edo. Aragua', 'marcos97@torrez.web.ve', '255-541-6067', '+58 499 301 69', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '85', '127575', 'Miguel Leo Cintrón', 'FEMENINO', '28', 'Callejón Quiroz, Piso 0, Parroquia Ruben Edo. Amazonas, 7735', 'mohamed60@gmail.com', '475-4137016', '+58 241 460 33', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '86', '712316', 'Salma Tapia', 'FEMENINO', '10', 'Callejón Jan, Hab. 20, Gómez de Mara Edo. Monagas, 7213', 'maria.hernandez@yahoo.com', '+58 407-977-43', '+58 255-308-03', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '87', '976233', 'Nil Negrón', 'FEMENINO', '28', 'Carretera Jordi Solorio, Piso 33, Carrero de Asis Edo. Trujillo', 'sfarias@latinmail.com', '435 9327651', '402 979 2052', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '88', '88617', 'Carlota Berta Elizondo', 'MASCULINO', '25', 'Callejón Ismael, Piso 86, Guillermo de Altagracia Edo. Carabobo', 'candelaria.marina@yahoo.com', '236-013-4727', '263-823-5585', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '89', '844974', 'Mara Ona Laureano', 'MASCULINO', '32', 'Calle Urías, Nro 41, Santa Diegode Mata Edo. Miranda, 6363', 'ceballos.helena@huerta.co.ve', '+58 4843060499', '+58 2968855255', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '90', '112603', 'Saul Velasco Hijo', 'FEMENINO', '4', 'Calle Javier Rolón, 38, Piso 5, Alonso del Tuy Edo. Yaracuy', 'luisa.ceballos@yahoo.es', '464 4664978', '456-571-5860', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '92', '917616', 'Dr. Domingo Expósito', 'MASCULINO', '20', 'Av. Ruben Guevara, Hab. 72, Villa Nayara Edo. Monagas', 'malave.mara@roca.info.ve', '425-438-0681', '2633533021', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '93', '133410', 'Oscar Francisco Berríos', 'MASCULINO', '12', 'Vereda Negrete, 3, Nro 5, Naiara de Mata Edo. Carabobo', 'clara.clemente@gmail.com', '284-457-5189', '+58 420-929-51', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '94', '698617', 'Dra. Ariadna Pardo', 'FEMENINO', '34', 'Avenida Antonia, Hab. 2, Puerto Alba Edo. Barinas, 6827', 'jdelafuente@santiago.net', '+58 417-661-15', '+58 436 865 46', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '95', '156439', 'Leire Lidia Saiz', 'MASCULINO', '22', 'Av. Vera, 40, Casa 89, La Gabriel Edo. Bolívar, 9458', 'avila.oswaldo@bustamante.net', '4468139129', '+58 272 477418', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '96', '30102', 'Adrian Raul Martí Olivas', 'MASCULINO', '31', 'Callejón Hugo Saucedo, Hab. 67, Jon del Tuy Edo. Delta Amacuro, 9236', 'eferrer@gallego.net', '261-585-1787', '+58 481-218-16', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '97', '329192', 'Ing. Ines Gallego', 'MASCULINO', '4', 'Callejón Zamudio, 599, Casa 00, Erika de Mata Edo. Lara', 'andrea40@hotmail.com', '+58 419-250-78', '+58 436-454568', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '98', '262447', 'Raul Andres Garibay Villalobos', 'MASCULINO', '8', 'Avenida Iria Quezada, 5, Casa 61, Valle Sergio Edo. Falcón, 1085', 'galvan.nora@lucas.co.ve', '+58 417 101109', '406 165 6064', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '99', '519016', 'Africa Blanca Pedraza', 'FEMENINO', '6', 'Cl. Isabel Anguiano, 3, Apto 2, Puerto Nicolas Edo. Aragua, 2735', 'clara.menendez@hotmail.es', '+58 4092298583', '+58 282-070-28', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '100', '786438', 'Oscar Leonardo Vigil', 'FEMENINO', '1', 'Vereda Arredondo, Casa 65, Ortega de Altagracia Edo. Portuguesa, 6468', 'roque.carlota@esquivel.co.ve', '487-2437294', '+58 283-761-85', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '101', '181014', 'Teresa Castaño Hijo', 'MASCULINO', '12', 'Av. Ander Rodrigo, Hab. 1, Acosta del Tuy Edo. Miranda, 0919', 'cerda.jose@terra.com', '+58 204 121447', '+58 271 364 01', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '102', '349723', 'Gonzalo Araña', 'MASCULINO', '19', 'Avenida Saavedra, Hab. 42, Terán del Valle Edo. Trujillo', 'jnoriega@villar.web.ve', '273-538-2702', '+58 2940163319', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '103', '813671', 'Sra. Celia Bueno', 'MASCULINO', '29', 'Vereda Roca, 2, Nro 9, Valle Bertadel Tuy Edo. Yaracuy, 8826', 'oswaldo.delatorre@terra.com', '433 074 7490', '+58 4460741456', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '104', '223379', 'Lola Adriana Archuleta', 'FEMENINO', '20', 'Av. Hugo Rojo, 17, Casa 5, La Javier Edo. Barinas, 2779', 'escudero.sara@hispavista.com', '2743550224', '+58 2007489860', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '105', '607', 'Lcdo. Gabriel Ornelas Hijo', 'FEMENINO', '8', 'Vereda Valencia, 6, Apto 2, Los Brunode Mara Edo. Mérida, 2155', 'xvillegas@hotmail.es', '433 6097782', '+58 224-608374', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '106', '885978', 'Ines Delgadillo Borrego', 'MASCULINO', '22', 'Vereda Sisneros, 37, Nro 00, San Alexde Altagracia Edo. Apure, 9690', 'erik.barajas@hotmail.com', '297 8164597', '+58 289 777702', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '107', '298168', 'Ing. Leonardo Zamudio', 'MASCULINO', '24', 'Av. Salas, Piso 0, Alejandro de Mata Edo. Guárico, 5886', 'dario.tomas@yahoo.es', '+58 272 445 63', '428-0367380', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '108', '202388', 'Alba Rosario', 'MASCULINO', '29', 'Callejón Isaac, 46, Nro 9, San Candela Edo. Mérida, 2561', 'isaac77@vidal.org.ve', '+58 487-350405', '+58 446-048-46', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '109', '964035', 'Lcdo. Leonardo Centeno', 'MASCULINO', '23', 'Vereda Naiara, Casa 98, Vicente de Altagracia Edo. Delta Amacuro, 5706', 'estrada.antonio@garrido.net.ve', '417 359 0034', '252-533-4899', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '110', '949558', 'Carlota Llorente Hijo', 'MASCULINO', '12', 'Callejón Alexia Ríos, Nro 36, Arevalo de Mata Edo. Sucre', 'roberto.morales@casares.net.ve', '407 960 5318', '+58 2030672151', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '111', '848469', 'Samuel Adrian Roldan Robles', 'FEMENINO', '30', 'Callejón Izan Lucas, 452, Hab. 42, Gabriel de Altagracia Edo. Yaracuy, 3165', 'franciscojavier98@hotmail.com', '+58 243-456228', '456 834 7049', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '112', '497073', 'Sr. Rafael Delao Hijo', 'FEMENINO', '22', 'Calle Alejandro, 34, Piso 3, Las Berta Edo. Distrito Capital', 'bella.miramontes@bonilla.net', '+58 490 101 08', '438 1590967', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '113', '610254', 'Srita. Ainara Ortíz Hijo', 'MASCULINO', '22', 'Avenida Naia, Piso 92, Villa Ian Edo. Trujillo, 3624', 'shernandez@yahoo.es', '+58 409 730151', '443 733 8772', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '114', '173566', 'Sra. Victoria Aguilar', 'FEMENINO', '31', 'Calle Victor Rangel, Piso 9, Benavídez del Valle Edo. Falcón, 6869', 'bjaimes@live.com', '444-3123993', '275 584 0709', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '115', '862580', 'Alonso Angel Villalpando', 'MASCULINO', '23', 'Cl. Gamez, Casa 1, San Nil Edo. Delta Amacuro, 5958', 'rosado.adrian@yahoo.com', '234 971 6177', '259-881-8182', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '116', '219434', 'Manuel Soriano', 'FEMENINO', '5', 'Carretera Alicia Bañuelos, Nro 7, Villa Naiara Edo. Portuguesa', 'miguel.santana@martin.org.ve', '4943020353', '263 081 4807', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '117', '455117', 'Luna Quezada', 'FEMENINO', '17', 'Av. Ariadna, Hab. 34, Vázquez de Mara Edo. Anzoátegui', 'martin17@terra.com', '258 931 0896', '+58 288-423619', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '118', '40729', 'Ismael Lucas Fajardo Nieto', 'MASCULINO', '6', 'Avenida Roberto Benítez, 7, Nro 55, Los Mireia Edo. Guárico, 5327', 'raul95@yahoo.com', '+58 251-897580', '442 649 2875', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '119', '15751', 'Dra. Victoria Blázquez Hijo', 'MASCULINO', '12', 'Cl. Ona, Nro 83, Puerto Isaac Edo. Zulia, 2999', 'dario49@hotmail.es', '264-5599734', '4908022081', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '120', '997848', 'Jana Pelayo Escobar', 'FEMENINO', '22', 'Avenida Rangel, 991, Casa 43, Ander de Mata Edo. Nueva Esparta, 9373', 'harmendariz@aguilar.com.ve', '208 032 3084', '+58 293-440195', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '121', '95791', 'Noa Rodrigo Ordóñez', 'FEMENINO', '18', 'Callejón Saul Córdova, Casa 1, Zúñiga de Asis Edo. Sucre, 9572', 'aguilar.joel@gmail.com', '+58 4351819053', '400-9937492', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '122', '570293', 'Carlota Rubio Tamayo', 'FEMENINO', '13', 'Callejón Barrios, 84, Nro 8, Puerto Javier Edo. Zulia', 'ariadna97@soler.web.ve', '+58 247-512909', '461 688 6564', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '123', '792916', 'Eduardo Bueno Hijo', 'MASCULINO', '24', 'Callejón Marco, Nro 49, Mares de Altagracia Edo. Yaracuy', 'ebustamante@martinez.info.ve', '+58 460 143 34', '+58 451 771934', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '124', '855877', 'Erik Aaron Montez', 'FEMENINO', '29', 'Vereda Sarabia, 34, Hab. 37, Puerto Ariadna Edo. Distrito Capital', 'claudia54@hotmail.com', '+58 200 838154', '299 4556199', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '125', '292266', 'Saul Carvajal', 'FEMENINO', '17', 'Vereda Palomo, Hab. 06, Raquel del Tuy Edo. Apure', 'meraz.alicia@vela.info.ve', '+58 489-159444', '+58 272-825-26', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '126', '133872', 'Javier Córdova Hijo', 'FEMENINO', '26', 'Carretera Antonio Zelaya, Piso 80, Gonzales de Asis Edo. Barinas', 'jaramillo.manuela@lugo.org.ve', '+58 433-550197', '+58 483-282775', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '127', '451488', 'Lcdo. Jordi Requena Hijo', 'MASCULINO', '7', 'Callejón Ríos, 5, Nro 22, Pau de Mara Edo. Portuguesa, 3430', 'leire.jaquez@gmail.com', '+58 455 052 03', '286 4508457', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '128', '306786', 'Aaron Leo Sotelo Loya', 'FEMENINO', '24', 'Vereda Reyes, 6, Piso 18, Orellana de Altagracia Edo. Nueva Esparta', 'saguilar@terra.com', '269-596-8054', '+58 296 866 05', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '129', '391568', 'Victoria Palomino', 'FEMENINO', '3', 'Vereda Aguirre, 695, Casa 2, Berta de Mara Edo. Distrito Capital, 8970', 'martina32@yahoo.com', '+58 236-486-40', '2516307032', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '130', '432652', 'Bruno Cuenca Hijo', 'MASCULINO', '17', 'Carretera Curiel, Nro 97, El Alonsodel Valle Edo. Bolívar', 'tlazaro@crespo.com.ve', '+58 288 185559', '+58 237 618 12', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '131', '860282', 'Ruben Solorio Hijo', 'MASCULINO', '12', 'Calle Cazares, Hab. 3, Lomeli de Mata Edo. Aragua', 'enrique.sotelo@nava.com', '469 639 5323', '+58 425-352-97', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '132', '454611', 'Lcdo. Juan Paredes', 'FEMENINO', '4', 'Av. Pau, Apto 35, San Elsa Edo. Falcón', 'izan07@riojas.org.ve', '+58 402 098 89', '488-002-3336', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '133', '101212', 'Isaac Erik Cortes Santillán', 'FEMENINO', '7', 'Avenida Zambrano, Piso 5, El Africa Edo. Nueva Esparta', 'domingo.montes@regalado.com.ve', '4954641630', '+58 488 507969', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '134', '466427', 'Juan Malave Hijo', 'FEMENINO', '6', 'Avenida Leire, Apto 4, El Mariodel Tuy Edo. Lara, 6258', 'magana.carla@hispavista.com', '4151938451', '+58 2550559083', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '135', '903535', 'Alma Cervántez Hijo', 'FEMENINO', '15', 'Carretera Gabriel Bermúdez, 273, Apto 9, Santa Miguelde Mata Edo. Guárico, 0133', 'bcasanova@live.com', '431-261-8435', '+58 2648595752', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '136', '378147', 'Patricia Oliver Hijo', 'FEMENINO', '31', 'Carretera Patiño, 9, Casa 51, Los Adamdel Tuy Edo. Amazonas, 5668', 'casas.blanca@live.com', '436-9023752', '487 443 8722', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '137', '683659', 'Jordi Garibay', 'MASCULINO', '12', 'Callejón Juan Ordóñez, Nro 6, El Izan Edo. Barinas, 2851', 'delgado.david@aleman.com.ve', '+58 420-796-05', '+58 251 783 65', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '138', '595231', 'Dra. Nayara Castañeda', 'MASCULINO', '13', 'Carretera Álvarez, Hab. 04, Zelaya del Tuy Edo. Cojedes', 'cesar.santos@gmail.com', '+58 279-444-09', '469 183 5929', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '139', '935714', 'Dr. Francisco Pineda Hijo', 'MASCULINO', '5', 'Callejón Rafael, Apto 15, El Beatrizdel Tuy Edo. Bolívar', 'kordonez@hotmail.es', '473 3886388', '+58 285-154-82', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '140', '652593', 'Marti Zepeda Hijo', 'MASCULINO', '27', 'Vereda Pol Burgos, 28, Nro 65, San Noeliade Asis Edo. Lara', 'xrey@frias.net', '437-207-2731', '2484780319', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '141', '758130', 'Ana Alonzo', 'MASCULINO', '15', 'Vereda Nahia, 108, Piso 31, La Laura Edo. Miranda', 'maria.saucedo@abad.org.ve', '439 8157580', '417 6216594', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '142', '174336', 'Francisco Adam Zarate', 'MASCULINO', '11', 'Carretera Adam, Casa 8, Parroquia Daniel Edo. Yaracuy, 5084', 'nabreu@pina.org', '+58 2177804675', '427 459 8097', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '143', '932824', 'Ander Domínguez Hijo', 'FEMENINO', '19', 'Cl. Cristina Gaona, 56, Nro 39, Canales del Valle Edo. Apure', 'nahia.delao@hernandes.org', '+58 405-352208', '202-0550127', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '144', '389742', 'Paola Núñez', 'MASCULINO', '7', 'Avenida Dario, Piso 0, Los Irenede Altagracia Edo. Distrito Capital, 0985', 'irene69@sandoval.co.ve', '+58 495 676 95', '+58 4719238919', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '145', '487257', 'Sra. Valeria Lucio Hijo', 'MASCULINO', '26', 'Vereda Concepción, 8, Hab. 17, Lorenzo de Asis Edo. Cojedes, 9875', 'tirado.andres@escamilla.net', '+58 236 695535', '+58 411-568-19', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '146', '712783', 'Jordi Arredondo Hijo', 'FEMENINO', '12', 'Calle Verduzco, Hab. 5, Raul de Asis Edo. Carabobo', 'carla07@hotmail.es', '206 5997922', '451 4156582', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '147', '857284', 'Dario Osorio', 'MASCULINO', '28', 'Av. Claudia Nieto, 596, Piso 7, Santa Javierde Mara Edo. Bolívar', 'anna.narvaez@live.com', '+58 457-535319', '445-558-6237', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '148', '745840', 'Nil Santos', 'MASCULINO', '25', 'Callejón Jimena, 5, Casa 98, Villa Valeria Edo. Táchira', 'bernal.alonso@gmail.com', '+58 456-626-28', '+58 459 938469', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '149', '518538', 'Srita. Alba Aguilera Hijo', 'FEMENINO', '17', 'Av. Acuña, 452, Piso 61, La Enrique Edo. Falcón, 7462', 'omar77@hotmail.com', '+58 2286943797', '+58 4335153682', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '150', '63727', 'Cesar Pichardo', 'MASCULINO', '1', 'Calle Solano, Casa 0, Pulido de Mata Edo. Delta Amacuro', 'vcalvillo@yahoo.es', '463-6997715', '+58 455 858202', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '151', '98212', 'Pablo Leo Barela Trejo', 'MASCULINO', '19', 'Calle Adam Villalba, Apto 80, Puerto Oliviade Asis Edo. Nueva Esparta, 9223', 'leonardo85@padilla.info.ve', '2799049542', '452 798 6483', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '152', '181596', 'Guillermo Adam Lerma Carrasco', 'FEMENINO', '8', 'Cl. Jan Saavedra, 66, Nro 55, Archuleta de Mara Edo. Yaracuy', 'ona20@gmail.com', '228-6411781', '+58 2611839302', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '153', '265610', 'Celia Lemus Hijo', 'MASCULINO', '32', 'Av. Roberto Lerma, 20, Apto 35, Las Nicolasdel Tuy Edo. Sucre', 'faleman@latinmail.com', '236 294 8099', '434 5465462', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '154', '257515', 'Pol Cristian Cuesta', 'MASCULINO', '30', 'Calle Miramontes, Nro 3, Valle de Mata Edo. Carabobo', 'alexia23@orellana.org', '250 691 9532', '434-5416921', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '155', '506531', 'Sara Erika Quesada', 'MASCULINO', '18', 'Callejón Hernádez, 313, Piso 0, La Lucas Edo. Monagas', 'lucia.cotto@ramos.net', '+58 473-431599', '+58 419 128 41', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '156', '581069', 'Rodrigo Almaraz Hijo', 'FEMENINO', '21', 'Avenida Naia Carrasco, 3, Casa 70, Valle Alexandra Edo. Sucre, 2056', 'eduardo97@latinmail.com', '249-209-6239', '+58 203 428 82', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '157', '541926', 'Carlos Mesa Hijo', 'MASCULINO', '4', 'Cl. Bustamante, Piso 0, Madrid del Tuy Edo. Delta Amacuro, 2840', 'puig.isaac@quintero.com.ve', '432-2293862', '4647774717', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '158', '411522', 'Dr. Jose Carranza', 'FEMENINO', '3', 'Cl. Manuel, 2, Hab. 4, Sáez de Mara Edo. Falcón', 'bermejo.leyre@vigil.com', '+58 445-750689', '263-4918649', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '159', '654473', 'Nil Alcala Hijo', 'MASCULINO', '17', 'Carretera Mas, 2, Hab. 7, Bueno del Valle Edo. Amazonas, 8541', 'hreyna@live.com', '+58 4774091577', '498 5737864', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '160', '730008', 'Manuel Alvaro Diez López', 'FEMENINO', '22', 'Av. Alba Sanches, Hab. 1, Bahena del Valle Edo. Guárico', 'pau55@terra.com', '+58 241-731810', '470-6196638', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '161', '671486', 'Lcda. Nadia Figueroa', 'FEMENINO', '26', 'Callejón Rafael Alaniz, Piso 83, San Mohamed Edo. Nueva Esparta', 'oscar.acuna@latinmail.com', '457-558-6022', '+58 255-998-02', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '162', '292917', 'Victoria Mar Muñoz', 'FEMENINO', '33', 'Vereda Teresa Alarcón, 6, Casa 53, San Juan Edo. Monagas, 2309', 'kcastaneda@quesada.net.ve', '+58 447 989 17', '+58 214 517540', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '163', '419818', 'Ing. Alicia Clemente', 'FEMENINO', '28', 'Calle Noelia, Piso 40, Blanca del Tuy Edo. Vargas, 5672', 'alexandra.pineiro@latinmail.com', '+58 2591331500', '270 888 4423', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '164', '688954', 'Ing. Francisco Peña Hijo', 'MASCULINO', '35', 'Carretera Pol, 195, Hab. 82, Los Carlade Mara Edo. Mérida', 'fpaz@montero.org.ve', '472-999-9556', '+58 4375000427', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '165', '887864', 'Joan Tamez Madrid', 'MASCULINO', '31', 'Vereda Cesar, 1, Casa 8, Quiroz de Mara Edo. Delta Amacuro, 6226', 'omar.arevalo@yahoo.com', '+58 4662651137', '+58 2952606230', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '166', '966008', 'Leo Mascareñas Hijo', 'MASCULINO', '1', 'Vereda Pol, 588, Casa 3, López del Valle Edo. Barinas, 9955', 'alba82@barajas.org', '266 178 2137', '231-6868696', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '167', '774016', 'Srita. Candela Serrato Hijo', 'MASCULINO', '13', 'Avenida Rafael Ramón, Nro 83, Elizondo de Mara Edo. Carabobo, 2325', 'cesar.carballo@vergara.org', '+58 476-690-72', '+58 267-474283', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '168', '576942', 'Sara Araña Hijo', 'FEMENINO', '13', 'Callejón Cardenas, 7, Nro 04, Cavazos de Altagracia Edo. Cojedes', 'luna86@hotmail.com', '4239495834', '+58 487 863941', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '169', '902038', 'Cristina Noa Olvera', 'MASCULINO', '30', 'Av. Cesar Crespo, Casa 8, Angela del Tuy Edo. Nueva Esparta, 0943', 'hvalenzuela@delrio.co.ve', '291-0341861', '+58 225-213456', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '170', '511389', 'Ariadna Julia Puga', 'MASCULINO', '16', 'Avenida Sandra Narváez, Casa 50, Puerto Almade Altagracia Edo. Zulia', 'oabad@linares.org', '+58 276 804 40', '4152499511', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '171', '52853', 'Ing. Marcos Aguilar Hijo', 'FEMENINO', '9', 'Calle Jordi Ponce, Casa 2, Villa Anade Asis Edo. Bolívar, 6053', 'muniz.nicolas@hurtado.org', '467 9776320', '2141098515', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '172', '865407', 'Laia Cervántez', 'FEMENINO', '28', 'Vereda Palomino, Nro 44, San Samueldel Tuy Edo. Trujillo, 0469', 'solis.eva@laboy.web.ve', '400-8392427', '+58 266-472-33', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '173', '366469', 'Andres Jesus Agosto', 'MASCULINO', '26', 'Av. Sevilla, 1, Nro 23, Santa Fatimadel Valle Edo. Vargas', 'cortes.rocio@live.com', '+58 241-543-93', '2193537613', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '174', '384781', 'Sra. Adriana Tovar', 'MASCULINO', '24', 'Cl. Marina, 3, Nro 11, Blasco de Asis Edo. Amazonas', 'nil30@cerda.web.ve', '+58 214-074-61', '+58 246 569265', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '175', '987710', 'Candela Pedroza Piña', 'MASCULINO', '6', 'Calle Nil Perales, Casa 99, Arias del Valle Edo. Mérida, 3961', 'emma.cepeda@olvera.org', '266-524-1401', '+58 404 663 69', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '176', '754052', 'Alejandro Villalpando', 'FEMENINO', '15', 'Cl. Helena, 80, Piso 4, Paola de Asis Edo. Apure', 'tceballos@gmail.com', '4144342172', '228 782 2404', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '177', '649343', 'Dra. Olivia Roybal Hijo', 'FEMENINO', '20', 'Vereda Paz, 1, Apto 45, Aparicio del Valle Edo. Lara, 8086', 'arroyo.guillermo@latinmail.com', '489-2201630', '+58 495-682-89', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '178', '68241', 'Dario Pedro Patiño Leal', 'FEMENINO', '24', 'Vereda Millán, Apto 4, Gurule de Altagracia Edo. Vargas, 5621', 'rodrigez.mar@live.com', '477 9528034', '+58 216-724-00', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '179', '56287', 'Ing. Olivia Casárez', 'FEMENINO', '23', 'Avenida Sara, Piso 10, Sierra de Asis Edo. Monagas', 'medina.beatriz@franco.com', '+58 267-110804', '4705167991', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '180', '392872', 'Laia Ana Valverde', 'MASCULINO', '27', 'Vereda Raul, 048, Piso 0, De la cruz del Tuy Edo. Portuguesa, 4381', 'diego.carrero@yahoo.es', '+58 490 214923', '274-2749370', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '181', '503815', 'Francisco Javier Soto', 'MASCULINO', '17', 'Callejón Leonardo Blasco, Nro 1, Los Sergiodel Valle Edo. Guárico, 6990', 'jordi.marquez@escobedo.co.ve', '472-968-7343', '480 564 3985', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '182', '226040', 'Miguel Echevarría', 'MASCULINO', '3', 'Carretera Jan, Casa 2, El Martina Edo. Portuguesa, 0538', 'negron.oscar@yahoo.es', '+58 282 112698', '+58 4979763880', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '184', '77058', 'Rocio Lucero Hijo', 'MASCULINO', '28', 'Avenida Lola Santillán, 28, Piso 6, Soriano de Altagracia Edo. Aragua, 5984', 'andres.cuevas@solorio.org', '247 953 7943', '225 4041818', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '185', '503420', 'Sra. Carlota Collazo Hijo', 'MASCULINO', '6', 'Cl. Laura Gallegos, 338, Piso 6, San Erika Edo. Barinas, 9944', 'claudia.nino@armenta.web.ve', '427-3406389', '285-0641501', 'ACTIVO', '2019-03-03', 'ANGEL' ),
( '186', '640454', 'Andres Andres Mascareñas Delrío', 'MASCULINO', '21', 'Calle Clara Altamirano, Apto 3, Oswaldo de Mata Edo. Guárico, 9384', 'montero.sebastian@terra.com', '5840 207-93-77', '4951 746-56-08', 'ACTIVO', '2019-03-03', '1' ),
( '187', '890506', 'Paola Yaiza Esparza', 'MASCULINO', '26', 'Av. Menéndez, Piso 9, Santa Iriade Asis Edo. Anzoátegui', 'rocio.montenegro@yahoo.com', '2770 337-68-83', '5820 961-23-71', 'ACTIVO', '2019-03-03', '1' ),
( '201', '923847', 'ANGEL ANTUNEZ', 'MASCULINO', '2', 'CALLE PRINCIPAL', 'angel@empresa.com', '2034 293-87-33', '2938 742-93-33', 'ACTIVO', '2019-03-03', '1' );
-- ---------------------------------------------------------


-- CREATE INDEX "Index_1" --------------------------------------
CREATE INDEX `Index_1` USING BTREE ON `tbpersonas`( `cargo_id` );
-- -------------------------------------------------------------


-- CREATE LINK "fk_cargoId" ------------------------------------
ALTER TABLE `tbpersonas`
	ADD CONSTRAINT `fk_cargoId` FOREIGN KEY ( `cargo_id` )
	REFERENCES `tbcargo`( `id` )
	ON DELETE Restrict
	ON UPDATE Restrict;
-- -------------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


