<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	// Incluimos el archivo fpdf
	require_once APPPATH."/third_party/fpdf/fpdf.php";

//Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
class Pdf extends FPDF {

	public function __construct() {
		parent::__construct();
	}

	//funciones para cambiar los encabezados de los reportes
	function SetDatosHeader($datosH){
		$this->datosHeader = $datosH;
	}

	//funciones para cambiar los encabezados de los reportes
	function GetDatosHeader(){
		return $this->datosHeader;
	}
	// El encabezado del PDF
	function Header() {
		if ($this->header == 1) {
			if ($this->PageNo()==1) {
				$this->Image('assets/img/user.png',10,5,10);
				$this->SetFont('Arial','B',11);
				$datosHeader = $this->GetDatosHeader();
				$titu = $datosHeader['Titulo'];
				$sti1 = $datosHeader['Subtit1'];
				$sti2 = $datosHeader['Subtit2'];
				$sti3 = $datosHeader['Subtit3'];
				$fhoy = date('d-m-Y H:i:s');
				$this->SetFont('','BU');
				$this->Cell(13);// separar la siguiente celda del margen izquierdo
				$this->Cell(25,8,iconv("UTF-8","ISO-8859-1","$titu"),0,0,'L');
				$this->Ln(4);
				$this->SetFont('','B');
				$this->Cell(13);// separar la siguiente celda del margen izquierdo
				$this->Cell(25,8,iconv("UTF-8","ISO-8859-1","$sti1"),0,0,'L');
				$this->Ln(0);
				$this->SetFont('Arial','B',8);
				$this->Cell(275,8,iconv("UTF-8","ISO-8859-1","FECHA: $fhoy"),0,0,'R');
			 } elseif($this->PageNo() >=1 ) {
				// el mismo encabezado de la primera pagina
				$this->Ln(7);
				$this->SetFont('Arial','B',6);
				$this->SetFillColor(229, 229, 229);
				$this->Cell(8,10,'Nro.','TL',0,'C','1');
				$this->MultiCell(8,5,'Ctrl Int.','TL',0,'C','1');
				$this->Cell(24,10,'ORIGEN','TL',0,'C','1');
				$this->Cell(20,10,'FECHA','TL',0,'C','1');
				$this->Cell(50,10,'EMPRESA IMPORTADORA','TL',0,'C','1');
				$this->Cell(25,10,'REPRESENTANTE','TL',0,'C','1');
				$this->MultiCell(26,5,'DATOS DEL CONDUCTOR / C.I','TL',0,'C','1');
				$this->MultiCell(26,10,'VEHICULO - PLACA','TL',0,'C','1');
				$this->Cell(37,10,'DESTINO','TL',0,'C','1');
				$this->Cell(55,10,'DIRECCION','TLR',0,'C','1');
				/*$this->Cell(30,10,'CONTACTO','TL',0,'C','1');
				$this->Cell(35,10,'MUNICIPIO','TL',0,'C','1');
				$this->Cell(40,10,'PARROQUIA','TL',0,'C','1');
				$this->Cell(8,10, 'ITEM','TL',0,'C','1');
				$this->Cell(80,10,'RUBRO','TL',0,'C','1');
				$this->Cell(35,10,'MARCA','TL',0,'C','1');
				$this->MultiCell(30,5,'PRESENT. DE LA UNIDAD','TL',0,'C','1');
				$this->MultiCell(20,5,'CANTIDAD DE UNIDADES','TL',0,'C','1');
				$this->SetFont('Arial','B',6);
				$this->MultiCell(32,3.30,'PRECIO DE VENTA MAXIMO EN BS POR UNID. DEL IMPORTADOR AL COMERCIO','TL',0,'C','1');
				$this->MultiCell(30,3.30,'PRECIO DE VENTA AL CONSUMIDOR FINAL EN BS POR UNID','TLR',0,'C','1');*/
				$this->Ln(10);
			}
	  } elseif ($this->header == 2) {
				$this->Image('assets/img/logos/icon-file.png',10,8,25);
				$this->SetFont('Arial','B',11);
				// recibo las variables para el header
				$datosHeader = $this->GetDatosHeader();
				$titu =	$datosHeader['Titulo'];
				$nrec =	$datosHeader['nrorec'];
				$cven =	$datosHeader['cedven'];
				$nven =	$datosHeader['nombven'];
				$fpco =	$datosHeader['fpagore'];
				$zona	=	$datosHeader['zonaven'];
				$mail =	$datosHeader['mailven'];
				$tpago= number_format($datosHeader['pgtotal'],2,',','.');
				$lpago= num_to_letras($datosHeader['pgtotal']);
				$tcve =	$datosHeader['tlfnoce'];
				$nrec = $datosHeader['nrorec'];
				$nr = str_pad($nrec, 5, '0', STR_PAD_LEFT);

				/*ENCABEZADO*/
				// preparar primera linea del encabezado
				$this->Ln(4);
				$this->SetFont('Arial','B',16);
				$this->Cell(65);
				$this->MultiCell(60,6,"$titu",0,0,'C');
				$this->SetFont('Arial','B',14);
				$this->Cell(25);
				$this->MultiCell(40,8,"NRO RECIBO",'LTR',0,'C');
				$this->Ln(8);
				$this->Cell(150);
				$this->Cell(40,8,"$nr",'BLR',0,'C');
				$this->Ln(10);

				// preparar la segunda linea del encabezado
				$this->SetFont('Arial','B',11);
				$this->Cell(102,4,"NOMBRE VENDEDOR:",0,0,'L');
				$this->Cell(8);
				$this->Cell(30,4,"ZONA",0,0,'C');
				$this->Cell(8);
				$this->Cell(42,4,"FECHA DE PAGO",0,0,'C');
				$this->Ln(6);
				$this->Cell(102,4,iconv("UTF-8","ISO-8859-1","$nven"),'B',0,'L');
				$this->Cell(8);
				$this->Cell(30,4,"$zona",'B',0,'C');
				$this->Cell(8);
				$this->Cell(42,4,"$fpco",'B',0,'C');
				$this->Ln(8);
				// preparar la tercera linea del encabezado
				$this->SetFont('Arial','B',11);
				$this->Cell(37,4,"HE RECIBIDO DE:",0,0,'L');
				$this->Cell(1);
				$this->Cell(152,4,"FRIOLKA C.A.",'B',0,'L');
				$this->Ln(8);

				$this->Cell(37,4,"LA CANTIDAD DE:",0,0,'L');
				$this->Cell(1);
				$str_w1 = $this->GetStringWidth(iconv("UTF-8","ISO-8859-1","$lpago"));
				if($str_w1 <= 149) {$hcel1 = 4;} else {$hcel1 = 8;}
				$this->MultiCell(152,$hcel1, iconv("UTF-8","ISO-8859-1","$lpago"),'B',0,'L');
				$this->Ln(9);

				/* TITULOS DE COLUMNAS */
				// preparar primera linea del encabezado del cuerpo
				$this->SetFont('Arial', 'B', 10);// Se define el formato de fuente: Arial, negritas
				$this->Cell(30,8,'Fecha Factura',1,0,'C','1');
				$this->Cell(40,8,'Nro Factura a Pagar',1,0,'C','1');
				$this->Cell(45,8,'Monto Neto Factura',1,0,'C','1');
				$this->Cell(25,8,'% Comision',1,0,'C','1');
				$this->Cell(50,8,'Monto Comision en Bs.',1,0,'C','1');
				$this->Ln(8);
		} elseif ($this->header == 3) {
			$this->Image('assets/img/logos/icon-file.png',10,4,25);
			// recibo las variables para el header
			$datosHeader = $this->GetDatosHeader();
			$emp = $datosHeader['Titulo'];
			$rif = $datosHeader['Subtit1'];

			/*ENCABEZADO*/
			// preparar primera linea del encabezado
			$this->SetFont('Arial','B',9);
			$this->Cell(31);
			$this->Cell(60,10,"$emp",0,0,'L');
			$this->Ln(4);
			$this->Cell(31);
			$this->Cell(60,10,"$rif",0,0,'L');
			$this->Ln(8);
			// preparar la segunda linea del encabezado
			$this->SetFont('Arial','B',11);
			$this->Cell(270,4,"REPORTE DE CLIENTES REGISTRADOS",0,0,'C');
			$this->Ln(8);

			/*
			 * TITULOS DE COLUMNAS
			 *
			 * $this->pdf->Cell(Ancho, Alto,texto,borde,posición,alineación,relleno);
			 */
			// el mismo encabezado de la primera pagina
			$this->SetFont('Arial','B',6);
			$this->SetFillColor(229, 229, 229);
			$this->Cell(6,10,'Nro.',1,0,'C','1');
			$this->Cell(15,10,'CEDULA',1,0,'C','1');
			$this->Cell(50,10,'NOMBRE',1,0,'C','1');
			$this->Cell(20,10,'TLFNO OFICINA',1,0,'C','1');
			$this->Cell(20,10,'TLFNO CELULAR',1,0,'C','1');
			$this->Cell(90,10,'DIRECCION CLIENTE',1,0,'C','1');
			$this->Cell(40,10,'CORREO ELECTRONICO',1,0,'C','1');
			$this->MultiCell(20,5,'REGISTRADO DESDE',1,0,'C','1');
			$this->Ln(10);//Se agrega un salto de linea
		}
	}

	// El pie del pdf
	function Footer(){
		if ($this->footer == 1) {
			$this->SetY(-15);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		} elseif ($this->footer == 2) {
			$this->SetY(-30);
			$this->SetFont('courier','I',5);
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		} elseif ($this->footer == 3) {
			$this->SetY(-12);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		}
	}


		// para usar el multicell sin el salto de pagina que viene predeterminado
	/**
	* MultiCell with alignment as in Cell.
	* @param float $w
	* @param float $h
	* @param string $text
	* @param mixed $border
	* @param int $ln
	* @param string $align
	* @param boolean $fill
	*/
	function MultiAlignCell($w,$h,$text,$border=0,$ln=0,$align='L',$fill=false)	{
    // Store reset values for (x,y) positions
    $x = $this->GetX() + $w;
    $y = $this->GetY();

    // Make a call to FPDF's MultiCell
    $this->MultiCell($w,$h,$text,$border,$align,$fill);

    // Reset the line position to the right, like in Cell
    if( $ln==0 ) {
        $this->SetXY($x,$y);
    }
	}

}