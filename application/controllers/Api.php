<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		 $this->load->model('Api_model');
	}

	function index() {

		$data = $this->Api_model->fetch_all();
		echo json_encode($data->result_array());
	}

	function insert() {

		$this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('apellido', 'Apellido', 'required');
		$this->form_validation->set_rules('cedula', 'cedula', 'required');
		$this->form_validation->set_rules('sexo', 'sexo', 'required');
		$this->form_validation->set_rules('direccion', 'direccion', 'required');
		$this->form_validation->set_rules('tlfnocel', 'tlfnocel', 'required');
		$this->form_validation->set_rules('tlfnocas', 'tlfnocas', 'required');
		$this->form_validation->set_rules('correo', 'correo', 'required');
		$this->form_validation->set_rules('cargo', 'cargo', 'required');

		if($this->form_validation->run()) {

			$data = array(
				'nombre'   => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
				'cedula'   => $this->input->post('cedula'),
				'sexo'     => $this->input->post('sexo'),
				'direccion'=> $this->input->post('direccion'),
				'tlfnocel' => $this->input->post('tlfnocel'),
				'tlfnocas' => $this->input->post('tlfnocas'),
				'correo'   => $this->input->post('correo'),
				'cargo'    => $this->input->post('cargo'),
			);

			$this->Api_model->insert_api($data);

			$array = array(
				'success' => true,
			);
			

		} else {

			$array = array(
				'error'           => true,
				'nombre_error'    => form_error('nombre'),
				'apellido_error'  => form_error('apellido'),
				'cedula_error'    => form_error('cedula'),
				'sexo_error'      => form_error('sexo'),
				'direccion_error' => form_error('direccion'),
				'tlfnocel_error'  => form_error('tlfnocel'),
				'tlfnocas_error'  => form_error('tlfnocas'),
				'correo_error'    => form_error('correo'),
				'cargo_error'     => form_error('cargo'),
			);
		}

		echo json_encode($array);
	}

	function fetch_single() {

		if($this->input->post('idper')) {

			$user_id = $this->input->post('idper');
			$data = $this->Api_model->fetch_single_user($user_id);

			foreach ($data as $row) {
				$output['nombre']    = row['nombre'];
				$output['nombre']    = row['apellido'];
				$output['cedula']    = row['cedula'];
				$output['sexo']      = row['sexo'];
				$output['direccion'] = row['direccion'];
				$output['tlfnocel']  = row['tlfnocel'];
				$output['tlfnocas']  = row['tlfnocas'];
				$output['correo']    = row['correo'];
				$output['cargo']     = row['cargo'];
			}

			echo json_encode($output);
		}
	}

	function update() {

		$this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('apellido', 'Apellido', 'required');
		$this->form_validation->set_rules('cedula', 'cedula', 'required');
		$this->form_validation->set_rules('sexo', 'sexo', 'required');
		$this->form_validation->set_rules('direccion', 'direccion', 'required');
		$this->form_validation->set_rules('tlfnocel', 'tlfnocel', 'required');
		$this->form_validation->set_rules('tlfnocas', 'tlfnocas', 'required');
		$this->form_validation->set_rules('correo', 'correo', 'required');
		$this->form_validation->set_rules('cargo', 'cargo', 'required');

		if($this->form_validation->run()) {

			$data = array(
				'nombre'   => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
				'cedula'   => $this->input->post('cedula'),
				'sexo'     => $this->input->post('sexo'),
				'direccion'=> $this->input->post('direccion'),
				'tlfnocel' => $this->input->post('tlfnocel'),
				'tlfnocas' => $this->input->post('tlfnocas'),
				'correo'   => $this->input->post('correo'),
				'cargo'    => $this->input->post('cargo'),
			);

			$this->Api_model->update_api($this->input->post('idper'),$data);

			$array = array(
				'success' => true,
			);

		} else {

			$array = array(
				'error'           => true,
				'nombre_error'    => form_error('nombre'),
				'apellido_error'  => form_error('apellido'),
				'cedula_error'    => form_error('cedula'),
				'sexo_error'      => form_error('sexo'),
				'direccion_error' => form_error('direccion'),
				'tlfnocel_error'  => form_error('tlfnocel'),
				'tlfnocas_error'  => form_error('tlfnocas'),
				'correo_error'    => form_error('correo'),
				'cargo_error'     => form_error('cargo'),
			);

			echo json_encode($array);
		}
	}

	function delete() {

		if($this->input->post('idper')){

			if($this->Api_model->delete_single_user($this->input->post('idper'))) {

				$array = array(

					'success' => true,
				);

			} else {

				$array = array(

					'error' => true,
				);
			}

			echo json_encode($array);
		}
	}

}
