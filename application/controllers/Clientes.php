<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends CI_Controller {

	public function __construct() {
		parent::__construct();
		 $this->load->model('Clientes_model');
	}

	//con esta función validamos y protegemos el buscador
	public function validar() {

		$this->form_validation->set_rules('buscando', 'buscador', 'required|min_length[2]|max_length[20]|trim|xss_clean');
		$this->form_validation->set_message('required', 'El %s no puede ir vacío!');
		$this->form_validation->set_message('min_length', 'El %s debe tener al menos %s carácteres');
		$this->form_validation->set_message('max_length', 'El %s no puede tener más de %s carácteres');
		if ($this->form_validation->run() == TRUE) {

			$buscador = $this->input->post('buscando');
			$this->session->set_userdata('buscando', $buscador);
			redirect('clientes/index');
		} else {

			//cargamos la vista y el array data
			$this->session->unset_userdata('buscando');
			redirect('clientes/index');
		}
	}

	// funcion inical
	function index() {

		$buscador = $this->session->userdata('buscando');
		$segmento = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		//$por_pagina = $this->input->post('limite'); //Número de registros mostrados por páginas
		$por_pagina = 10; //Número de registros mostrados por páginas
		$config['base_url'] = base_url() . 'clientes/index'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
		$config['total_rows'] = $this->Clientes_model->busqueda($buscador); //calcula el número de filas
		$config['per_page'] = $por_pagina; //Número de registros mostrados por páginas
		$config["uri_segment"] = 3;
		$config['num_links'] = 5; //Número de links mostrados en la paginación
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = 'Primera';
		$config['prev_tag_open'] = '<li class="paginate_button previous">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Siguiente';
		$config['next_tag_open'] = '<li class="paginate_button next">';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="paginate_button active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="paginate_button ">';
		$config['num_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li class="paginate_button">';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="paginate_button">';
		$config['last_tag_close'] = '</li>';
		$config['first_link'] = 'Inicio';
		$config['last_link'] = 'Ultima';

		$this->pagination->initialize($config);
		//el array con los datos a paginar ya preparados
		$datos["resultados"] = $this->Clientes_model->total_posts_paginados($buscador, $por_pagina, $segmento);

		/*$sections = array(
			'config'  => TRUE,
			'queries' => TRUE,
			'controller_info' => TRUE,
			'uri_strig' => TRUE,
			'session_data' => TRUE

			);
		$this->output->set_profiler_sections($sections);
		$this->output->enable_profiler(TRUE);*/

		// cargamos la vista principal
		$this->load->view('plantillas/front_end/header');
		$this->load->view('front-end/clientes/tablacliente', $datos);
		$this->load->view('plantillas/front_end/footer');
	}

	// para consultar si la cedula ya existe
	public function consultcedula(){
		
		$cedula = $this->input->post('cedula');

		$consulta = $this->Clientes_model->consultar_cedula($cedula);
			//echo $consulta;
			if ($consulta == 0 ) {
				$valid = json_encode(array('valid' => TRUE));
				//return $valid;
				echo $valid;
			} else if ($consulta >= 1){
				$valid = json_encode(array('valid' => FALSE));
				//return $valid;
				echo $valid;
			}
	}

	// para buscar los datos del cliente
	public function buscarCliente(){
		$cedula = $this->input->get('term',TRUE);
		if ($cedula){
			$valores = $this->Clientes_model->consultar_cliente($cedula);
			return $valores;
		}
	}

	//con esta función registramos clientes
	public function regcliente() {

		$this->load->view('front-end/clientes/regcliente');
	}

	//con esta función modificar cliente
	public function editcliente() {

		$this->load->view('front-end/clientes/editcliente');
	}

	//con esta función abrimos el modal para editar matricula
	public function borraru() {

		$this->load->view('front-end/clientes/borrar_cliente');
	}

	//con esta función insertaremos los comentarios
	public function insertar_registro() {

		//si se ha pulsado el botón submit validamos el formulario con codeIgniter
		$nome = $this->input->post('nombre').' '.$this->input->post('apellido');
		$cede = str_replace('.', '',$this->input->post('cedula'));
		$sexo = $this->input->post('sexo');
		$dire = $this->input->post('direccion');
		$tcle = $this->input->post('tlfnocel');
		$tcae = $this->input->post('tlfnocas');
		$emae = $this->input->post('correo');
		$carg = $this->input->post('cargo');

		//conseguimos la hora de nuestro país,
		$fec = date('Y-m-d');
		$ope = '1';

		// inserta los datos arreglados en la tabla de estudiante
		$insert_e  = $this->Clientes_model->insertar_cliente($nome,$cede,$sexo,$dire,$tcle,$tcae,$emae,$carg,$fec,$ope);

		//$this->output->enable_profiler(TRUE);

		if ($insert_e == 'error') {

			$this->session->set_flashdata('message', 'Ha Ocurrido un error al Insertar.!');
			$this->session->set_flashdata('tipomsg','danger');
			redirect('clientes/index');

		} elseif ($insert_e == 'correcto') {

			$this->session->set_flashdata('message', 'La Inserción del Cliente se hizo correctamente.!');
			$this->session->set_flashdata('tipomsg','info');
			redirect('clientes/index');
		}
	}

	//con esta función insertaremos los comentarios
	public function modificar_registro() {

		//print_r($_POST);
		//si se ha pulsado el botón submit validamos el formulario con codeIgniter
		$idcl = $this->input->post('idempcli');
		$nome = $this->input->post('nombre').' '.$this->input->post('apellido');
		$cede = str_replace('.', '',$this->input->post('cedula'));
		$sexo = $this->input->post('sexo');
		$dire = $this->input->post('direccion');
		$tcle = $this->input->post('tlfnocel');
		$tcae = $this->input->post('tlfnocas');
		$emae = $this->input->post('correo');
		$crgi = $this->input->post('cargo');

		//conseguimos la hora de nuestro país,
		$fec = date('Y-m-d');
		$ope = '1';

		// lo que envia el boton modificar
		$boton = $this->input->post('Modificar');


		if ($boton == "Modificar") {
			// modifica los datos arreglados en la tabla de estudiante
			$modif_e  = $this->Clientes_model->modificar_cliente($idcl,$nome,$cede,$sexo,$dire,$tcle,$tcae,$emae,$crgi,$fec,$ope);

			//print_r($modif_e);
			if ($modif_e == 'correcto') {

				$this->session->set_flashdata('message', 'La Inserción del Cliente se hizo correctamente.!');
				$this->session->set_flashdata('tipomsg','info');
				redirect('clientes/index');

			} elseif($modif_e == 'error') {

				$this->session->set_flashdata('message', 'Ha Ocurrido un error al Insertar.!');
				$this->session->set_flashdata('tipomsg','danger');

				redirect('clientes/index');
			}
		}
	}

	// para inactivar un registro de la tabla de clientes
	public function borrar_cliente(){

		$opc = $this->input->post('optionsRadios');
		if ($opc =='Si') {
			$idcl = $this->input->post('idempcli');
		}

		//echo 'idgme: '.$idg;
		$borra  = $this->Clientes_model->borrar_usuario($idcl);

		if ($borra == 'error') {
			$this->session->set_flashdata('message', 'No se puede borrar el Cliente..!');
			$this->session->set_flashdata('tipomsg','danger');
			redirect('clientes/index');
		} else {
			$this->session->set_flashdata('message', 'Se borró correctamente el Cliente.!');
			$this->session->set_flashdata('tipomsg','success');
			redirect('clientes/index');
		}
	}

}