<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Repo extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Repo_model');
		// Se carga la libreria fpdf
		$this->load->library('pdf');
	}

	// menu principal
	public function index(){

		// cargamos la vista principal
		$this->load->view('plantillas/front_end/header');
		$this->load->view('reportes/menu_reportes');
		$this->load->view('plantillas/front_end/footer');
	}

	// imprimir reporte de clientes
	public function rpt_clientes_pdf() {

		$this->load->view('reportes/rpt_clientes_pdf');

	}


}
