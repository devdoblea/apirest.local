<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Testapi extends CI_Controller {

	function index() {

		$this->load->view('tablacliente');
	}

	function action() {

		if( $this->input->post('data_action') ) {

			$data_action = $this->input->post('data_action');

			// Crear un registro
			if($data_action == "Insert") {

				$api_url = base_url("api/insert");

				$form_data = array(
					'nombre'   => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'cedula'   => $this->input->post('cedula'),
					'sexo'     => $this->input->post('sexo'),
					'direccion'=> $this->input->post('direccion'),
					'tlfnocel' => $this->input->post('tlfnocel'),
					'tlfnocas' => $this->input->post('tlfnocas'),
					'correo'   => $this->input->post('correo'),
					'cargo'    => $this->input->post('cargo'),
				);

				$cliente = curl_init($api_url);
				curl_setopt($cliente, CURLOPT_POST, true);
				curl_setopt($cliente, CURLOPT_POSTFIELDS, $form_data);
				curl_setopt($cliente, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($cliente);
				curl_close($cliente);

				echo $response;
			}

			// Editar el registro seleccionado
			if($data_action == "Edit") {

				$api_url = base_url("api/update");

				$form_data = array(
					'idper'    => $this->input->post('user_id'),
					'nombre'   => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'cedula'   => $this->input->post('cedula'),
					'sexo'     => $this->input->post('sexo'),
					'direccion'=> $this->input->post('direccion'),
					'tlfnocel' => $this->input->post('tlfnocel'),
					'tlfnocas' => $this->input->post('tlfnocas'),
					'correo'   => $this->input->post('correo'),
					'cargo'    => $this->input->post('cargo'),
				);

				$cliente = curl_init($api_url);
				curl_setopt($cliente, CURLOPT_POST, true);
				curl_setopt($cliente, CURLOPT_POSTFIELDS, $form_data);
				curl_setopt($cliente, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($cliente);
				curl_close($cliente);

				echo $response;
			}

			// Borrar el registro especifico
			if($data_action == "Delete") {

				$api_url = base_url("api/delete");

				$form_data = array(
					'idper' => $this->input->post('user_id'),
				);

				$cliente = curl_init($api_url);
				curl_setopt($cliente, CURLOPT_POST, true);
				curl_setopt($cliente, CURLOPT_POSTFIELDS, $form_data);
				curl_setopt($cliente, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($cliente);
				curl_close($cliente);

				echo $response;

			}

			// Editar usuario
			if($data_action == "fetch_single") {

				$api_url = base_url("api/fetch_single");

				$form_data = array(
					'idper'    => $this->input->post('user_id'),
					'nombre'   => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'cedula'   => $this->input->post('cedula'),
					'sexo'     => $this->input->post('sexo'),
					'direccion'=> $this->input->post('direccion'),
					'tlfnocel' => $this->input->post('tlfnocel'),
					'tlfnocas' => $this->input->post('tlfnocas'),
					'correo'   => $this->input->post('correo'),
					'cargo'    => $this->input->post('cargo'),
				);

				$cliente = curl_init($api_url);
				curl_setopt($cliente, CURLOPT_POST, true);
				curl_setopt($cliente, CURLOPT_POSTFIELDS, $form_data);
				curl_setopt($cliente, CURLOPT_RETURNTRANSFER, true);

				$response = curl_exec($cliente);
				curl_close($cliente);
				echo $response;
			}

			// Mostrar los latos en una tabla
			if($data_action == "fetch_all") {

				$api_url = base_url("api/index");
				$cliente = curl_init($api_url);
				curl_setopt($cliente, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($cliente);
				curl_close($cliente);
				$result = json_decode($response);
				$output = '';
				if(count($result) > 0) {
					
					foreach ($result as $row) {

						$output .='
							<tr>
								<td style="text-align:center; vertical-align:middle;">'.$row->idper. '</td>
								<td style="text-align:center; vertical-align:middle;">'.$row->cedula.'</td>
								<td style="text-align:left; vertical-align:middle;">'  .$row->nombre.'</td>
								<td style="text-align:center; vertical-align:middle;">'.$row->tlfnocel.'</td>
								<td style="text-align:center; vertical-align:middle;"><button type="button" class="btn btn-primary hidden-print" data-toggle="modal" data-target="#ventanaEditar" data-ides="'.$row->idper.'" title="Editar este Cliente"><i class="fa fa-edit "></i></button></td>
								<td style="text-align:center; vertical-align:middle;"><button type="button" class="btn btn-danger hidden-print" data-toggle="modal" data-target="#ventanaBorrar" data-ides="'.$row->idper.'" title="Borrar este Cliente"><i class="fa fa-remove "></i></button></td>
							</tr>
						'; 
					}
				} else {

						$output = '
							<tr>
								<td colspan="4" align="center">
									No hay datos para mostrar
								</td>
							</tr>
						';
				}
			}

			echo $output;
		}
	}

}

?>