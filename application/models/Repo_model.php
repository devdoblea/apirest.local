<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Repo_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	// reporte de clientes registrados
	function listar_clientes_registrados() {
		$this->db->order_by('nombre', 'ASC');
		$consulta = $this->db->get('tbpersonas');
		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
			$datos[] = $fila;
		}
			return $datos;
		} else {
			return 'nada';
		}
	}
}