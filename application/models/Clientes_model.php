<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->db->query("SET sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'");
	}

	//obtenemos el total de filas para hacer la paginación del buscador
	function busqueda($buscador) {
		$this->db->join('tbcargo', 'tbpersonas.cargoId = tbcargo.id');
		$this->db->where("statusper != 'SUSPENDIDO'");
		$this->db->like('cedula', $buscador);
		$this->db->or_like('nombre', $buscador);
		$consulta = $this->db->get('tbpersonas');
		return $consulta->num_rows();
	}

	//obtenemos todos los posts a paginar con la función
	//total_posts_paginados pasando lo que buscamos, la cantidad por página y el segmento
	//como parámetros de la misma
	function total_posts_paginados($buscador, $por_pagina, $segmento) {
		$this->db->join('tbcargo', 'tbpersonas.cargoId = tbcargo.id');
		$this->db->where("statusper != 'SUSPENDIDO'");
		$this->db->like('cedula', $buscador);
		$this->db->or_like('nombre', $buscador);
		$this->db->order_by('idper', 'DESC');
		$consulta = $this->db->get('tbpersonas', $por_pagina, $segmento);
		if ($consulta->num_rows() > 0) {
			foreach ($consulta->result() as $fila) {
				$datos[] = $fila;
			}
			return $datos;
		}
	}

	// listar los importadores
	public function consultar_idcliente($id) {
		$this->db->join('tbcargo', 'tbpersonas.cargoId = tbcargo.id');
		$this->db->where('idper',$id);
		$resultados = $this->db->get('tbpersonas');

		return $resultados;
	}

	function consultar_cedula($cedula) {
		$this->db->where('cedula',$cedula);
		$query = $this->db->get('(SELECT * FROM `tbpersonas` WHERE tbpersonas.statusper != "INACTIVO" ) AS tbpersonas');
			$q = $query->num_rows();
			return $q;
	}

	// insertar cliente
	public function insertar_cliente($nome,$cede,$sexo,$dire,$tcle,$tcae,$emae,$carg,$fec,$ope) {

		$data = array(
			'nombre' 	    => $nome,
			'cedula'	    => $cede,
			'sexo' 		  	=> $sexo,
			'direccion'		=> $dire,
			'tlfnocel'	  => $tcle,
			'tlfnocas'  	=> $tcae,
			'correo'   		=> $emae,
			'cargoId'   	=> $carg,
			'statusper'   => 'ACTIVO',
			'fechain'  		=> $fec,
			'operador'		=> $ope
		);

		// verifico que ya no exista un registro igual
		$this->db->where('cedula',$rife);
		$query = $this->db->get('tbpersonas');

			if($query->num_rows() >= 1 ) {

				return 'error';

			} else {

				$this->db->insert('tbpersonas', $data);
				return 'correcto';

			}

	}

	// modificar cliente
	public function modificar_cliente($idcl,$nome,$cede,$sexo,$dire,$tcle,$tcae,$emae,$crgi,$fec,$ope) {
		$data = array(
			'nombre' 	    => $nome,
			'cedula'	    => $cede,
			'sexo' 		  	=> $sexo,
			'direccion'		=> $dire,
			'tlfnocel'	  => $tcle,
			'tlfnocas'  	=> $tcae,
			'correo'   		=> $emae,
			'cargoId'   	=> $crgi,
			'statusper'   => 'ACTIVO',
			'fechain'  		=> $fec,
			'operador'		=> $ope
		);

			// verificamos si se insertaron los registros
			$this->db->where('idper',$idcl);
			if (! $this->db->update('tbpersonas', $data)){

				return 'error';

			 } else {

				return 'correcto';

			}
	}

	// borra (mas bien cambia el status) el registro del usuario
	public function borrar_usuario($idcl) {

		$this->db->where('idper',$idcl);
		if ($this->db->delete('tbpersonas')) {

			return 'correcto';

		} else {

			return 'error';
		}
	}

}