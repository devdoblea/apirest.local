<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	//obtenemos el total de filas para hacer la paginación del buscador
	function fetch_all() {

		// $data = array(
		// 	'nombre' 	    => $nome,
		// 	'cedula'	    => $cede,
		// 	'sexo' 		  	=> $sexo,
		// 	'direccion'		=> $dire,
		// 	'tlfnocel'	  => $tcle,
		// 	'tlfnocas'  	=> $tcae,
		// 	'correo'   		=> $emae,
		// 	'cargoId'   	=> $crgi,
		// 	'statusper'   => 'ACTIVO',
		// 	'fechain'  		=> $fec,
		// 	'operador'		=> $ope
		// );

		$this->db->join('tbcargo', 'tbpersonas.cargoId = tbcargo.id');
		$this->db->order_by('idper', 'DESC');
		return $this->db->get('tbpersonas');
	}

	function insert_api($data) {

		$this->db->insert('tbpersonas', $data);
	}

	function fetch_single_user($user_id) {

		$this->db->where('idper', $user_id);
		$query = $this->db->get('tbpersonas');
		return $query->result_array();
	}

	function update_api($user_id, $data) {

		$this->db->where('idper', $user_id);
		$this->db->update('tbpersonas', $data);
	}

	function delete_api($user_id) {

		$this->db->where('idper', $user_id);
		$this->db->delete('tbpersonas');
	}

}