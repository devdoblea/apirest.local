<?php
	function format_phone($country, $phone) {
	  	$function = 'format_phone_' . $country;
		  if(function_exists($function)) {
		    return $function($phone);
		  }
		return $phone;
	}
	 
	function format_phone_us($phone) {
	  // note: making sure we have something
	  if(!isset($phone{3})) { return ''; }
	  // note: strip out everything but numbers 
	  $phone = preg_replace("/[^0-9]/", "", $phone);
	  $length = strlen($phone);
		switch($length) {
			case 7:
				return preg_replace("/([0-9]{4})([0-9]{4})/", "$1-$2", $phone);
			 	break;
			case 10:
				return preg_replace("/([0-9]{4})([0-9]{3})([0-9]{2})([0-9]{2})/", "($1) $2-$3", $phone);
				break;
			case 11:
				return preg_replace("/([0-9]{0})([0-9]{4})([0-9]{3})([0-9]{4})/", "$1($2) $3-$4", $phone);
				break;
			default:
			    return $phone;
			break;
		}
	}

	// funcion que intercambia la fecha a formato mysql o humano
	function cambfecha($fecha) {
		if ($fecha == ''){
			$fech = '';
			$fnac = '';	
			return false;
		} else {
			$fech = explode('-',$fecha);
			$fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];

			return $fnac;
		}
	}

	function zerofill($valor, $longitud){
		$res = str_pad($valor, $longitud, '0', STR_PAD_LEFT);

		return $res;
	}

	// funcion que calcula la edad segun fecha dada
	function calculaEdad($fnac){
		// calcular edad
		list($Y,$m,$d) = explode("-",$fnac);
		$edad = date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y;

		return $edad;

	}

	// funcion para convertir fecha en fecha larga
	// Parámetros: string $data:  Fecha en formato dd/mm/aaaa o timestamp
	//             int    $tipus: Tipo de fecha (0-timestamp, 1-dd/mm/aaaa)
	//// Retorna:    string  Fecha en formato largo (x, dd mm de yyyy)
	function fecha_larga($data, $tipus=1){
		if ($data != '' && $tipus == 0 || $tipus == 1){
			$setmana = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
			$mes     = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
			if ($tipus == 1){
				preg_match('/([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})/', $data, $data);
				$data = mktime(0,0,0,$data[2],$data[1],$data[3]);
			}
			return $setmana[date('w', $data)].', '.date('d', $data).' '.$mes[date('m',$data)-1].' de '.date('Y', $data);
		} else {
			return 0;
		}
	}

	// funcion para saber cuanto tiempo pasó desde una fech y hora determinada
	/**
    * (desde[, hasta])
    *
    * Devuelve la diferencia textual entre dos fechas.
    */
	function time_ago($date) {
	   $timestamp = strtotime($date);	
	   
	   $strTime = array("segundo", "minutos", "horas", "dia", "mes", "año");
	   $length = array("60","60","24","30","12","10");

	   $currentTime = time();
	   if($currentTime >= $timestamp) {
			$diff     = time()- $timestamp;
			for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
			$diff = $diff / $length[$i];
			}

			$diff = round($diff);
			return 'Hace '.$diff . " " . $strTime[$i] . "(s) atras";
	   }
	}

	// funcion para separar
	function partir_texto($texto) {
		$split = explode('-',$texto);
		$parte = $split[0];

		return $parte;
	}

	// para acomodar el resultado de los telefonos
	function camelize($word) {
		$vowels = array("/", "-", "(", ")");
	   return str_replace($vowels, '', $word);
	}