<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3> REPORTES <small></small> </h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Reportes Estructurales <small></small></h2>
						<div class="clearfix"></div>
					</div>

					<div class="x_content">

						<div class="row">

							<p>Reportes Generales</p>

							<div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
								<a href="/repo/rpt_clientes_pdf" id="reportedirecto" data-repo="rpt_clientes_pdf" data-title="Reporte General de Clientes" title="Generar Reporte General de Clientes Registrados" target="_blank">
									<div class="tile-repos text-center">
										<div class="count">
											<i class="fa fa-2x fa-suitcase"></i>
											<h4><i class="fa fa-file-pdf-o"></i> Reporte de Clientes</h4>
										</div>
									</div>
								</a>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar estud-->
<div class="modal fade in" id="ventanaPeriodo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="ct text-center">
			</div>
		</div>
	</div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar estud-->
<div class="modal fade in" id="ventanaGenerando" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="ct text-center">
					<h2>Generando Reporte</h2>
			</div>
		</div>
	</div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar estud-->
<div class="modal fade in" id="ventanaPeriodo2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="ct text-center">
			</div>
		</div>
	</div>
</div>
<script>
	$('#ventanaPeriodo').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var repo = button.data('repo'); // Extract info from data-* attributes
		var nomb = button.data('title'); // Extract info from data-* attributes
		var modal = $(this);
		var dataString = { repo: repo, nomb: nomb};
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>repo/pidefechas",
				data: dataString,
				success: function (data) {
					//console.log(data);
					modal.find('.ct').html(data);
				},
				error: function(err) {
					consume_alert()
					alert('PideFecha: '+JSON.stringify(err['statusText']));
				}
			});
	});
</script>
<script>
	$('#ventanaPeriodo2').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var repo = button.data('repo'); // Extract info from data-* attributes
		var nomb = button.data('title'); // Extract info from data-* attributes
		var modal = $(this);
		var dataString = { repo: repo, nomb: nomb};
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>repo/pidefechasvend",
				data: dataString,
				success: function (data) {
					//console.log(data);
					modal.find('.ct').html(data);
				},
				error: function(err) {
					consume_alert()
					alert('PideFechaVend: '+JSON.stringify(err['statusText']));
				}
			});
	});
</script>