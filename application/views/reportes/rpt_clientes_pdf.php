<?php
	//print_r($_POST);

	// en caso de recargar la pagina o que no llegue nada por post entonces...
/*	if(!$emp) {
		echo "	<script>
							alert('No se puede recargar la pagina');
							window.close();
						</script>
					";
	}*/

	// saco los datos que debo colocar en el encabezado
		$datosH = array(
			'Titulo'  => iconv("UTF-8","ISO-8859-1",'EMPRESA TEST'),
			'Subtit1' => iconv("UTF-8","ISO-8859-1",'J-12345678-9'),
		);

		// Creacion del PDF

		/*
		 * Se crea un objeto de la clase Pdf, recuerda que la clase Pdf
		 * heredó todos las variables y métodos de fpdf
		 */
		$this->pdf = new Pdf();

		// asigno el valor a los encabezados
		$this->pdf->SetDatosHeader($datosH);

		// cargar un encabezado diferente
		$this->pdf->header = 3;
		// cargar un footer diferente
		$this->pdf->footer = 3;
		/* Se define el titulo, márgenes izquierdo, derecho y
		 * el color de relleno predeterminado
		 */
		$this->pdf->SetTitle("Reporte de Clientes");
		$this->pdf->SetTopMargin(4);
		$this->pdf->SetLeftMargin(4);
		$this->pdf->SetRightMargin(10);

		// Agregamos la pagina que va a ser escrita
		$this->pdf->AddPage('L','letter');  // para imprimir la pagina en un tamaño personalizado
		// Define el alias para el número de página que se imprimirá en el pie
		$this->pdf->AliasNbPages();

		// Se define el formato de fuente: Arial, negritas, tamaño 9
		$this->pdf->SetFont('Arial', '', 7);
		$this->pdf->SetFillColor(224,235,255); // para intercambiar los colores de las filas
    $this->pdf->SetTextColor(0); // color del texto
		$fill = false; // para intercalar el color en las filas

		// traigo los datos de los importadores
		$total = 0;
		$nrox = 1; // contador de lineas
		$imps = $this->Repo_model->listar_clientes_registrados();
		if ($imps != 'nada') {
			foreach ($imps as $r) {
				$this->pdf->SetFont('Arial', '', 7);
				$feccli = cambfecha($r->fechain);
				$this->pdf->Cell(6,10,  iconv("UTF-8","ISO-8859-1","$nrox"),1,0,'C',$fill);
				$this->pdf->Cell(15,10, iconv("UTF-8","ISO-8859-1","$r->cedula"),1,0,'C',$fill);
				$this->pdf->Cell(50,10, iconv("UTF-8","ISO-8859-1","$r->nombre"),1,0,'C',$fill);
				$this->pdf->Cell(20,10, iconv("UTF-8","ISO-8859-1","$r->tlfnocel"),1,0,'C',$fill);
				$this->pdf->Cell(20,10, iconv("UTF-8","ISO-8859-1","$r->tlfnocas"),1,0,'C',$fill);
				$str_w3 = $this->pdf->GetStringWidth(iconv("UTF-8","ISO-8859-1","$r->direccion"));
				if($str_w3 <= 90) {$hcel3 = 10;} else {$hcel3 = 5;}
				$this->pdf->MultiCell(90,$hcel3,iconv("UTF-8","ISO-8859-1","$r->direccion"),1,0,'C',$fill);

				$this->pdf->Cell(40,10, iconv("UTF-8","ISO-8859-1","$r->correo"),1,0,'C',$fill);
				$this->pdf->Cell(20,10, iconv("UTF-8","ISO-8859-1","$feccli"),1,0,'C',$fill);


				// para enumerar las filas de los datos mostrados
				$nrox++;
				//Se agrega un salto de linea
				$this->pdf->Ln(10);//Se agrega un salto de linea
				$fill = !$fill; // para cambiar el color de la linea
			}

		} else {
			$this->pdf->Ln(10);//Se agrega un salto de linea
			$this->pdf->SetFont('Arial', 'B', 20);
			$this->pdf->Cell(200,10,"NO HAY DATOS PARA LAS FECHAS SELECCIONADAS",0,0,'C');
		}
		/*
		 * Se manda el pdf al navegador
		 *
		 * $this->pdf->Output(nombredelarchivo, destino);
		 *
		 * I = Muestra el pdf en el navegador
		 * D = Envia el pdf para descarga
		 *
		 */
		$this->pdf->Output('Reporte_Depositos_Diarios_'.date('d-m-Y H:i:s').'.pdf', 'I');
?>