<!-- page content -->
<div class="right_col" role="main">
	<div class="">

		<div class="page-title">
			<div class="title_left">
				<h3>Clientes <small>Registre aqui un cliente</small></h3>
			</div>

			<div class="title_right">
				<div class="col-md-12 col-sm-12 col-xs-12 form-group">
					<div class="input-group pull-right">
						<?php $bagrega = array (
							'name'    => 'agregar',
							'id'      => 'agregar',
							'type'    => 'button',
							'content' => '<i class="fa fa-plus-circle"></i> Agregar Persona',
							'class'   => 'btn btn-success form-control',
							'title'   => 'Agregar',
							'data-toggle' => 'modal',
							'data-target' => '#ventanaAgregar',
						);
						echo form_button($bagrega);
						?>
					</div>
				</div>
			</div>

			<div class="title_right">
				<div class="col-md-8 col-sm-5 col-xs-12 form-group pull-left top_search">
					<?php echo form_open(base_url().'clientes/validar','class="formulario"') ?>
						<div class="input-group">
							<?php echo form_input('buscando',set_value('buscando'),'class="form-control" id="buscando" name="buscando" accept-charset="utf-8"	placeholder="Buscar por..." autocomplete="off" autocapitalize="off" autocorrect="off" spellcheck="false" maxlength="10"');?>
							<span class="input-group-btn">
								<button type="submit" id="buscar" name="buscar" class="btn btn-default"><i class="fa fa-search"></i></button>
							</span>
						</div>
					<?php echo form_close() ?>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<table class="table table-bordered table-hover table-responsive" role="grid">
										<thead>
											<tr style="vertical-align:middle;">
												<th class="text-center">Id.</th>
												<th class="text-center">Cédula/R.I.F:</th>
												<th class="text-center" style="width: 300px;">Persona</th>
												<th class="text-center">Telefono <br>Cel</th>
												<th class="text-center" colspan="2">Acción</th>
											</tr>
										</thead>
										<tbody id="apitabla">
											
										</tbody>
									</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- /page content -->
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div class="modal fade bs-example-modal-lg" id="ventanaEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content ct">
		</div>
	</div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div class="modal fade bs-example-modal-lg" id="ventanaAgregar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content ct">
		</div>
	</div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div class="modal fade bs-example-modal-lg" id="ventanaBorrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content ct">
		</div>
	</div>
</div>
<script>
	$('#ventanaAgregar').on('show.bs.modal', function(event) {
		var modal = $(this);
		$.ajax({
			url: "<?php echo base_url();?>clientes/regcliente",
			success: function (data) {
				modal.find('.ct').html(data);
			},
			error: function(err) {
				alert('VentanaAgrega: '+JSON.stringify(err['statusText']));
			}
		});
	});
</script>
<script>
	// cuando haga clic en el boton editar
	$('#ventanaEditar').on('show.bs.modal', function(event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var ides = button.data('ides'); // Extract info from data-* attributes
		var modal = $(this);
		var dataString = {ides: ides}
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>clientes/editcliente",
				data: dataString,
				success: function (data) {
					//console.log(data);
					modal.find('.ct').html(data);
				},
				error: function(err) {
					alert('VentanaEditar: '+JSON.stringify(err['statusText']));
				}
			});
	});
</script>
<script>
	$('#ventanaBorrar').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var recipient = button.data('borrar') // Extract info from data-* attributes
		var modal = $(this);
		var dataString = 'idempcli=' + recipient;
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()?>clientes/borraru',
			data: dataString,
			cache: false,
				success: function(data) {
					//console.log(data);
					modal.find('.ct').html(data);
				},
				error: function(err) {
					alert('ventanBorrar: '+JSON.stringify(err['statusText']));
				}
		});
	});
</script>
<script>
	 var form = document.createElement("form"); // crear un form
		with(form) {
		setAttribute("name", "myform"); //nombre del form
		setAttribute("action", ""); // action por defecto
		setAttribute("method", "post"); // method POST }
		}
		var input = document.createElement("input"); // Crea un elemento input
		with(input) {
		setAttribute("name", "nroctrl_int"); //nombre del input que va a pasar el valor a la otra pagina
		setAttribute("type", "hidden"); // tipo hidden
		setAttribute("value", ""); // valor por defecto
		}

		form.appendChild(input); // añade el input al formulario
		document.getElementsByTagName("body")[0].appendChild(form); // añade el formulario al documento

		window.onload=function(){
			var my_links = document.getElementsByTagName("a");
			for (var a = 0; a < my_links.length; a++) {
				if (my_links[a].name=="imprimir") my_links[a].onclick = function() {
					document.myform.action=this.href;
					document.myform.nroctrl_int.value=this.rel;
					document.myform.submit();
					return false;
				}
			}
		}
</script>
<script type="text/javascript">
	
	$(document).ready(function(){
		
		function fetch_data(){
			$.ajax({
				url: "<?php echo base_url();?>testapi/action",
				method: "POST",
				data: { data_action:'fetch_all' },
				success: function(data) {

					$('#apitabla').html(data);
				}
			});
		}

		fetch_data();
	});

	// Con esto se puede agregar via AJAX
	$('#add').on('click', function(){
		$('#user_form')[0].reset();
		$('.modal-title').text("Agregar Cliente");
		$('#action').val('Agregar');
		$('#data_action').val('Insertar');
		$('#userModel').modal('show');
	});

	$(document).on('submit', '#user_form', function(event){

		event.preventDefault();

		$.ajax({
			url: "<?php echo base_url();?>testapi/action",
			method: "POST",
			data: $(this).serialize(),
			dtaType: "json",
			success: function(data) {
				if(data.success){
					$('#user_form')[0].reset();
					$('#userModel').modal('show');
					fetch_data();
					if($('#data_action').val() == "Insert"){
						$('#success_message').html(
							'<div class="alert alert-success">Data Insertada</div>'
						);
					}
				}
				if(data.error) {
					$('#error_nombre').html(data.nombre_error);
					$('#error_telefono').html(data.cedula_error);
					$('#error_telefono').html(data.telefono_error);
				}
			}
		});
	});

	$(document).on('click', '.edit', function(){

		var user_id = $(this).attr('idper');

		$.ajax({
			url: "<?php echo base_url();?>testapi/action",
			method: "POST",
			data: { user_id:user_id, data_action:'Edit'},
			dtaType: "json",
			success: function(data) {
				$('#userModel').modal('show');
				$('#nombre').val(data.nombre);
				$('#apellido').val(data.apellido);
				$('.model-title').text('Editar Usuario');
				$('#user_id').val(user_id);
				$('#action').val('Editar');
				$('#data_action').val('Editar');
			}
		});
	});

	$(document).on('click', '.delete', function(){
		
		var user_id = $(this).attr('idper');

		if(confirm("Esta seguro que desea borrar este registro?")) {

			$.ajax({
				url: "<?php echo base_url();?>testapi/action",
				method: "POST",
				data: { user_id:user_id, data_action:'Delete'},
				dtaType: "json",
				success: function(data) {
					if(data.success) {

						$('#success_message').html('<div class="alert alert-success">Dato Borrado</div>');
						fetch_data();	
					}
				}
			});
		}
	});

</script>