<?php
	$id = $this->input->post('idempcli',TRUE);
	//echo 'Valor= '.$id;
?>
<form id="form-modificar" action="<?php echo base_url();?>clientes/borrar_cliente" method="post" role="form">

	<div class="modal-header bg-red">
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
		</button>
			<h4 class="modal-title text-center" id="myModalLabel">
				BORRAR CLIENTE Nro.: <?php echo $id;?>
			</h4>
	</div>

	<div class="modal-body">

		<div class="form-group col-md-12 col-sm-12 col-xs-12 text-center">
			<h2>Desea BORRAR este cliente<br>
					Si: <input type="radio" id="optionsRadios" name="optionsRadios" value="Si"><br>
					<input type="hidden" name="idempcli" id="idempcli" value="<?php echo $id;?>" />
			</h2>
		</div>

	</div>

	<div class="modal-footer col-md-12 col-sm-12 col-xs-12">
		<button type="submit" name="Borrar" class="btn btn-danger" value="Borrar" title="Borrar CLiente" >Borrar</button>
	</div>

</form>