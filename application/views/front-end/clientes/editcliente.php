<?php
	$id = $this->input->post('ides');
	$resultados = $this->Clientes_model->consultar_idcliente($id);
		foreach ( $resultados->result() as $r ) {
			$idcl = $r->idper;
			$part = explode(' ',$r->nombre, 2);
			$nome = $part[0];
			if($part[1] != '' | $part[1] != NULL ) { 
				$apel = $part[1]; } 
			else { 
				$apel = '';
		  };
			$cede = $r->cedula;
			$sexo = $r->sexo;
			$dire = $r->direccion;
			$emae = $r->correo;
			$tcle = $r->tlfnocel;
			$tcae = $r->tlfnocas;
			$carg = $r->cargo;
			$crgi = $r->cargo_id;
		}
?>
<!-- Nuevo DatetimePicker-->
<link href="<?php echo base_url()?>assets/vendors/DateTimePicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<!-- CSS para los que salga de JQuery sobre todo para el autocomplete-->
<link href="<?php echo base_url()?>assets/vendors/jquery-ui/css/jquery-ui.1.12.1.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/vendors/jquery-ui/css/jquery-ui.autocomplete.css" rel="stylesheet">
<!-- FormValidation -->
<link href="<?php echo base_url()?>assets/vendors/FormValidation/formValidation.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/vendors/FormValidation/formValidation.css" rel="stylesheet" type="text/css" />

<form id="form-modificar" action="<?php echo base_url();?>clientes/modificar_registro" method="post" role="form">
	<div class="modal-header bg-primary">
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
		</button>
			<h4 class="modal-title text-center" id="myModalLabel">
				MODIFICAR CLIENTES
				<input type="hidden" name="idempcli" id="idempcli" value="<?php echo $idcl?>"/>
			</h4>
	</div>

	<div class="modal-body">
		<div class="form-group col-md-4 col-sm-6 col-xs-6">
			<label>Nombre:</label>
				<input type="text" name="nombre" id="nombre" class="form-control" maxlength="30" value="<?php echo $nome?>" onchange="javascript:this.value=this.value.toUpperCase();" />
		</div>
		<div class="form-group col-md-5 col-sm-6 col-xs-6">
			<label>Apellido:</label>
				<input type="text" name="apellido" id="apellido" class="form-control" maxlength="30" value="<?php echo $apel?>" onchange="javascript:this.value=this.value.toUpperCase();" />
		</div>
		<div class="form-group col-md-3 col-sm-6 col-xs-6">
			<label>Cédula:</label>
			<input type="text" name="cedula" id="cedula" class="form-control" maxlength="6" value="<?php echo $cede?>" data-mask-rif />
		</div>
		<div class="form-group col-md-4 col-sm-6 col-xs-6">
			<label>Sexo:</label>
				<select name="sexo" id="sexo" class="form-control" placeholder="Sexo">
					<option value="<?php echo $sexo?>"><?php echo $sexo?></option>
					<option value="MASCULINO">MASCULINO</option>
					<option value="FEMENINO" >FEMENINO</option>
				</select>
		</div>
		<div class="form-group col-md-8 col-sm-12 col-xs-12">
			<label>Dirección:</label>
				<textarea style="text-align:justify;" name="direccion" id="direccion" class="form-control" rows="1" cols="55" onkeydown="if(this.value.length >= 200){ alert('Has superado el tama&ntilde;o m&aacute;ximo permitido en la dirección'); return false; }" onchange="javascript:this.value=this.value.toUpperCase();"><?php echo $dire?></textarea>
		</div>
		<div class="clearfix"></div>
		<div class="form-group col-md-4 col-md-offset-1 col-sm-6 col-xs-6">
			<label>Telefono Celular: </label>
				<input type="text" name="tlfnocel" id="tlfnocel" class="form-control" maxlength="14" value="<?php echo $tcle?>" data-mask-tel />
		</div>
		<div class="form-group col-md-4 col-md-offset-2 col-sm-6 col-xs-6">
			<label>Telefono Casa: </label>
				<input type="text" name="tlfnocas" id="tlfnocas" class="form-control" maxlength="14" value="<?php echo $tcae?>" data-mask-tel />
		</div>
		<div class="clearfix"></div>
		<div class="form-group col-md-6 col-sm-6 col-xs-6">
			<label>Correo Electrónico:</label>
				<input type="email" name="correo" id="correo" class="form-control" maxlength="60" value="<?php echo $emae?>" placeholder="Coloque un correo valido aqui" value="notiene@empresa.com" autocomplete="off"/>
		</div>
		<div class="form-group col-md-6 col-sm-6 col-xs-6">
			<label>Cargo:</label>
				<select name="cargo" id="cargo" class="form-control" placeholder="Cargo">
					<option value="<?php echo $crgi;?>"><?php echo $carg;?></option>
					<?php
						$query = $this->db->get('tbcargo');
						foreach ($query->result() as $r) {
							echo '<option value="'.$r->id.'">'.$r->cargo.'</option>';
						}
					?>
				</select>
		</div>

	</div>

	<div class="modal-footer col-md-12">
		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		<button type="submit" name="Modificar" class="btn btn-primary" value="Modificar" title="Modificar Datos" >Modificar</button>
	</div>
</form>

<!-- Para el autocomplete -->
<script src="<?php echo base_url()?>assets/vendors/jquery-ui/js/jquery-ui.1.12.1.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url()?>assets/vendors/jquery-inputmask/inputmask.min.js"></script>
<script src="<?php echo base_url()?>assets/vendors/jquery-inputmask/jquery.inputmask.min.js"></script>
<!-- formValidation-->
<script src="<?php echo base_url()?>assets/vendors/FormValidation/formValidation.js"></script>
<script src="<?php echo base_url()?>assets/vendors/FormValidation/formValidation_bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/vendors/FormValidation/formValidation_language_es_ES.js"></script>
<!-- Nuevo DatetimePicker-->
<script src="<?php echo base_url()?>assets/vendors/DateTimePicker/js/moment.min.js"></script>
<script src="<?php echo base_url()?>assets/vendors/DateTimePicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/vendors/DateTimePicker/js/bootstrap-datetimepicker.es.js"></script>
<!--Jquery.Number-->
<script src="<?php echo base_url()?>assets/vendors/jquery-number/jquery.number.js"></script>
<script>
	// Campo numerico
	$('.decimal-2-places').number( true, 2, ',', '.' );
	$('.positive-integer').number( true, 0, ',', '.' );
	//Mascara de Telefono
	$("[data-mask-tel]").inputmask("9999 999-99-99");
	//Mascara de RIF
	$("[data-mask-rif]").inputmask("999999");
	// validacion de campos
	$('#form-modificar').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		err: {
			container: 'popover'
		},
		excluded: ':disabled',
		fields: {
			nombre: {
				validators: {
					notEmpty: {
						message: 'El Nombre es requerido'
					},
					regexp: {
						regexp: /^[a-zA-ZáéíóúñÁÉÍÓÚÑ\s]+$/i,
						message: 'Solo puede contener letras'
					}
				}
			},
			apellido: {
				validators: {
					notEmpty: {
						message: 'El Apellido es requerido'
					},
					regexp: {
						regexp: /^[a-zA-ZáéíóúñÁÉÍÓÚÑ\s]+$/i,
						message: 'Solo puede contener letras'
					}
				}
			},
			cedula: {
				validators: {
					notEmpty: {
						message: 'Cedula es requerido'
					},
					regexp: {
						regexp: /^[0-9]{6}$/i,
						message: 'Solo puede contener Numeros Permitidos'
					},
				 }
			},
			sexo: {
				validators: {
					notEmpty: {
						message: 'Sexo es requerido'
					},
				}
			},
			direccion: {
				validators: {
			 		notEmpty: {
				 		message: 'La Direccion es requerida y no puede ir vacia'
			 		},
			 		stringLength: {
						max: 100,
						message: 'Ha llegado al limite de este campo'
			 		},
		 		}
	 		},
			tlfnocel: {
				 message: 'El Teléfono Celular no es valido',
				 validators: {
					 notEmpty: {
						 message: 'El teléfono Celular es requerido y no puede ir vacio'
					 },
					 regexp: {
						 regexp: /^\b\d{4}[ ]?\d{3}[-]?\d{2}[-]?\d{2}\b/,
						 message: 'El Teléfono Celular Principal solo puede contener números'
					 }
				 }
			},
			tlfnocas: {
				 message: 'El Teléfono Oficina Principal no es valido',
				 validators: {
					 notEmpty: {
						 message: 'El teléfono Oficina es requerido y no puede ir vacio'
					 },
					 regexp: {
						 regexp: /^\b\d{4}[ ]?\d{3}[-]?\d{2}[-]?\d{2}\b/,
						 message: 'El Teléfono Oficina solo puede contener números'
					 }
				 }
			},
			correo: {
				validators: {
					notEmpty: {
						message: 'El correo es requerido y no puede ir vacio'
					},
			 		emailAddress: {
						message: 'El correo electronico no es valido'
			 		}
		 		}
	 		},
			cargo: {
			 validators: {
				 notEmpty: {
					 message: 'Cargo es requerido'
				 },
			 }
		 	},
		}
	});
</script>