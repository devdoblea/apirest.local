<!-- Recoger / colapsar la barra lateral--
<script src="<?php //echo base_url()?>assets/js/recoge_sidebar.js"></script>-->
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="row top_tiles">
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a href="" name="agregar" id="agregar" title="Agregar" data-toggle="modal" data-target="#ventanaAgregar">
					<div class="tile-stats">
						<div class="icon"><i class="glyphicon glyphicon-import"></i></div>
						<?php $cuantasfact = $this->Facturas_model->consultar_facturas_estadisticas();?>
						<div class="count"><?php echo $cuantasfact;?></div>
						<h3>Registrar Factura</h3>
						<p>Registre una nueva factura pendiente por pagar. El sistema se encargará de procesarla</p>
					</div>
				</a>
			</div>
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a href="/facturas/index" >
					<div class="tile-stats">
						<div class="icon"><i class="fa fa-suitcase"></i></div>
						<?php
							$this->db->where('statuspago','PAGADA');
							$q = $this->db->get('tbfacturas');
						?>
						<div class="count"><?php echo $q->num_rows()?></div>
						<h3>Facturas Cobradas</h3>
						<p>Registre los Depósitos realizados. Tenga a la mano el Nro. de factura a cobrar.</p>
					</div>
				</a>
			</div>
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a href="/cheqdev/index" >
					<div class="tile-stats">
						<div class="icon"><i class="fa fa-building-o"></i></div>
						<?php
							$this->db->where('statusdep','DEVUELTO');
							$q = $this->db->get('tbdepoypagos');
						?>
						<div class="count"><?php echo $q->num_rows()?></div>
						<h3>Cheques Devueltos</h3>
						<p>Registre o vea cuales cheques se han devuelto. Vea cual cliente tiene cheques devueltos</p>
					</div>
				</a>
			</div>
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a href="/comisiones/index" >
					<div class="tile-stats">
						<div class="icon"><i class="glyphicon glyphicon-piggy-bank"></i></div>
						<?php
							$this->db->select('tbcomipagadas.nroctrl');
							$this->db->group_by('tbcomipagadas.nroctrl');
							$cuantascomi = $this->db->get('tbcomipagadas');
						?>
						<div class="count"><?php echo $cuantascomi->num_rows()?></div>
						<h3>Pago de Comisiones</h3>
						<p>Registre el pago de una comisión. Solo se muestran comisiones para facturas pagadas.</p>
					</div>
				</a>
			</div>
		</div>


		<div class="row">
			<div class="col-md-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Facturacion del Mes <small>Progreso Semanal</small></h2>
						<div class="filter">
							<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
								<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
								<span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<div class="col-md-12 col-sm-12 col-xs-12">

							<div class="demo-container" style="height:280px">
								<div id="placeholder33x" class="demo-placeholder"></div>
							</div>
							<div class="tiles">
								<div class="col-md-4 tile">
									<span>Total Fact. Pendientes</span>
									<h2>231,809</h2>
								</div>
								<div class="col-md-4 tile">
									<span>Total Facturas Pagadas</span>
									<h2>$231,809</h2>
								</div>
								<div class="col-md-4 tile">
									<span>Total Depositos</span>
									<h2>231,809</h2>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>

		<input type="hidden" id="fini" name="fini" value="2016-08-15">
		<input type="hidden" id="ffin" name="ffin" value="2016-08-31">
	</div>
</div>
<!-- /page content -->
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div class="modal fade bs-example-modal-lg" id="ventanaAgregar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content ct">
		</div>
	</div>
</div>
<script>
	$('#ventanaAgregar').on('show.bs.modal', function(event) {
		var modal = $(this);
		$.ajax({
			url: "<?php echo base_url();?>facturas/regfact",
			success: function (data) {
				modal.find('.ct').html(data);
			},
			error: function(err) {
				alert(err);
			}
		});
	});
</script>
<!-- Chart.js -->
<script src="<?php echo base_url()?>assets/vendors/Chart.js/Chart.min.js"></script>
<!-- Flot -->
<script src="<?php echo base_url()?>assets/vendors/Flot/jquery.flot.js"></script>
<script src="<?php echo base_url()?>assets/vendors/Flot/jquery.flot.time.js"></script>
<script>
	/*$(document).ready(function() {
		var fini = $("#fini").val();
		var ffin = $("#ffin").val();
		var dataStrings = { fini:fini, ffin:ffin };

		$.ajax({
			url: "<?php echo base_url()?>main/datos_guias",
			type:'POST',
			data: dataStrings,
			dataType: 'json',
			complete: function(data){
				//console.log(data);
				$(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
					type: 'bar',
					height: '140',
					barWidth: 20,
					colorMap: {
						'10': '#a1a1a1'
					},
					barSpacing: 2,
					barColor: '#26B99A'
				});
			}
		});
	});*/
	$(document).ready(function() {
		//define chart clolors ( you maybe add more colors if you want or flot will add it automatic )
		var chartColours = ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'];

		//generate random number for charts
		randNum = function() {
			return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
		};

		var d1 = [];
		//var d2 = [];

		//here we generate data for chart
		for (var i = 0; i < 30; i++) {
			d1.push([new Date(Date.today().add(i).days()).getTime(), randNum() + i + i + 10]);
			//    d2.push([new Date(Date.today().add(i).days()).getTime(), randNum()]);
		}

		var chartMinDate = d1[0][0]; //first day
		var chartMaxDate = d1[20][0]; //last day

		var tickSize = [1, "day"];
		var tformat = "%d/%m/%y";

		//graph options
		var options = {
			grid: {
				show: true,
				aboveData: true,
				color: "#3f3f3f",
				labelMargin: 10,
				axisMargin: 0,
				borderWidth: 0,
				borderColor: null,
				minBorderMargin: 5,
				clickable: true,
				hoverable: true,
				autoHighlight: true,
				mouseActiveRadius: 100
			},
			series: {
				lines: {
					show: true,
					fill: true,
					lineWidth: 2,
					steps: false
				},
				points: {
					show: true,
					radius: 4.5,
					symbol: "circle",
					lineWidth: 3.0
				}
			},
			legend: {
				position: "ne",
				margin: [0, -25],
				noColumns: 0,
				labelBoxBorderColor: null,
				labelFormatter: function(label, series) {
					// just add some space to labes
					return label + '&nbsp;&nbsp;';
				},
				width: 40,
				height: 1
			},
			colors: chartColours,
			shadowSize: 0,
			tooltip: true, //activate tooltip
			tooltipOpts: {
				content: "%s: %y.0",
				xDateFormat: "%d/%m",
				shifts: {
					x: -30,
					y: -50
				},
				defaultTheme: false
			},
			yaxis: {
				min: 0
			},
			xaxis: {
				mode: "time",
				minTickSize: tickSize,
				timeformat: tformat,
				min: chartMinDate,
				max: chartMaxDate
			}
		};
		var plot = $.plot($("#placeholder33x"), [{
			label: "Email Sent",
			data: d1,
			lines: {
				fillColor: "rgba(150, 202, 89, 0.12)"
			}, //#96CA59 rgba(150, 202, 89, 0.42)
			points: {
				fillColor: "#fff"
			}
		}], options);
	});
</script>
<!-- /Flot -->
<!-- bootstrap-daterangepicker -->
<script type="text/javascript">
	$(document).ready(function() {

		var cb = function(start, end, label) {
			console.log(start.toISOString(), end.toISOString(), label);
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		};

		var optionSet1 = {
			startDate: moment().subtract(29, 'days'),
			endDate: moment(),
			minDate: '01/01/2012',
			maxDate: '12/31/2015',
			dateLimit: {
				days: 60
			},
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'left',
			buttonClasses: ['btn btn-default'],
			applyClass: 'btn-small btn-primary',
			cancelClass: 'btn-small',
			format: 'MM/DD/YYYY',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Clear',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		};
		$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
		$('#reportrange').daterangepicker(optionSet1, cb);
		$('#reportrange').on('show.daterangepicker', function() {
			console.log("show event fired");
		});
		$('#reportrange').on('hide.daterangepicker', function() {
			console.log("hide event fired");
		});
		$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
			console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
		});
		$('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
			console.log("cancel event fired");
		});
		$('#options1').click(function() {
			$('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
		});
		$('#options2').click(function() {
			$('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
		});
		$('#destroy').click(function() {
			$('#reportrange').data('daterangepicker').remove();
		});
	});
</script>
<!-- /bootstrap-daterangepicker -->