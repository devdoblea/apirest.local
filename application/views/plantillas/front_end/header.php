<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?php echo base_url()?>assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>TEST | BIENVENIDO</title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	<!-- Bootstrap -->
	<link href="<?php echo base_url()?>assets/vendors/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/vendors/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo base_url()?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo base_url()?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- bootstrap-wysiwyg -->
  <link href="<?php echo base_url()?>assets/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
  <!--Pnotify para las notificaciones y mensajes de error-->
	<link href="<?php echo base_url()?>assets/vendors/pnotify/pnotify.css" media="all" rel="stylesheet" />
	<!-- Custom Theme Style -->
	<link href="<?php echo base_url()?>assets/css/custom.min.css" rel="stylesheet">
	
	<!-- jQuery -->
	<script src="<?php echo base_url()?>assets/vendors/jquery/dist/jquery.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url()?>assets/vendors/bootstrap/js/bootstrap.js"></script>
	<!-- FastClick -->
	<script src="<?php echo base_url()?>assets/vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="<?php echo base_url()?>assets/vendors/nprogress/nprogress.js"></script>
	<!-- Para los Alerts de javascript-->
	<script src="<?php echo base_url()?>assets/vendors/pnotify/pnotify.js"></script>
	<?php
		// mensaje de alerta en caso de error mostrado con alertify.js
		if (!$this->session->flashdata('message')) {
			echo '';
		} else {
				if(!$this->session->flashdata('tipomsg')) {
					$tipo = "error";
				} else {
					$tipo = $this->session->flashdata('tipomsg');
				}

			echo '
				<script>
					PNotify.prototype.options.styling = "bootstrap3";
					$(function(){
							var animate_in = $("#animate_in").val(),animate_out = $("#animate_out").val();
							new PNotify({
									title: "Mensaje del Sistema",
									text: "'.$this->session->flashdata('message').'",
									type: "'.$tipo.'",
									addclass: "stack-custom",
									delay:2000,
									mobile:{swipe_dismiss:true,styling:true},
									desktop: {desktop: true,fallback: true},
									animate: {
						        animate: true,
										in_class: animate_in,
      							out_class: animate_out
						    }
							});
					});
				</script>
			';
		}
	?>
</head>

<body id="cuerpo" class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<?php echo anchor('clientes/index', '<i class="fa fa-paw"></i> <span>EMPRESA</span>','class="site_title"');?>
					</div>

					<div class="clearfix"></div>

					<!-- menu profile quick info -->
					<div class="profile">
						<div class="profile_pic">
							<img src="<?php echo base_url()?>assets/img/avatar-hombre.png" alt="..." class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Bienvenido,</span>
							<h2>
								USUARIO
							</h2>
						</div>
					</div>
					<!-- /menu profile quick info -->

					<br />

					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3>General</h3>
							<ul class="nav side-menu">
								<li><a><i class="fa fa-edit"></i> Registrar <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><?php echo anchor('clientes/index', '<i class="fa fa-suitcase"></i> Clientes');?></li>
									</ul>
								</li>
								<li><?php echo anchor('repo/index', '<i class="fa fa-clone"></i> Reportes <span class="fa fa-chevron-left"></span>');?></li>
							</ul>
						</div>

					</div>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Settings">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="FullScreen">
							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Lock">
							<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
						</a>
						<a href="<?php echo base_url()?>auth/logout" data-toggle="tooltip" data-placement="top" title="Logout">
								<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
						</a>
					</div>
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
							<script>
								$("body").on("load", function() {
								  $("#menu_toggle" ).trigger("click");
								});
							</script>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class="">
								<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<img src="<?php echo base_url()?>assets/img/avatar-hombre.png" alt="">
									USUARIO
									<span class=" fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu pull-right">
									<li><a href="javascript:;"><i class="fa fa-user pull-right"></i> Perfil</a></li>
									<li>
										<a href="javascript:;">
											<!--<span class="badge bg-red pull-right">50%</span>-->
											<i class="fa fa-cog pull-right"></i>
											<span>Configuraciones</span>
										</a>
									</li>
									<li><a href="<?php echo base_url()?>auth/logout"><i class="fa fa-sign-out pull-right"></i> Salir</a></li>
								</ul>
							</li>

						</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->